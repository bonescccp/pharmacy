package by.epam.pharmacy.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * Order class
 */
public class Order implements Identifiable, Serializable {

    private static final long serialVersionUID = 1L;

    private final int id;
    private final int userId;
    private final Medicine medicine;
    private final int quantity;
    private final boolean payed;

    public Order(int id, int userId, Medicine medicine, int quantity, boolean payed) {
        this.id = id;
        this.userId = userId;
        this.medicine = medicine;
        this.quantity = quantity;
        this.payed = payed;
    }

    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public Medicine getMedicine() {
        return medicine;
    }

    public int getQuantity() {
        return quantity;
    }

    public boolean isPayed() {
        return payed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Order order = (Order) o;
        return id == order.id &&
                userId == order.userId &&
                quantity == order.quantity &&
                payed == order.payed &&
                Objects.equals(medicine, order.medicine);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, medicine, quantity, payed);
    }
}
