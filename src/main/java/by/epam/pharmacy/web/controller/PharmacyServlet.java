package by.epam.pharmacy.web.controller;

import by.epam.pharmacy.dao.DaoFactory;
import by.epam.pharmacy.dao.connection.ConnectionPool;
import by.epam.pharmacy.dao.connection.ProxyConnection;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandFactory;
import by.epam.pharmacy.web.command.CommandResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Main controller
 */
public class PharmacyServlet extends HttpServlet {
    private static final String COMMAND_PARAM_NAME = "command";
    private static Logger logger = LogManager.getLogger(PharmacyServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String command = request.getParameter(COMMAND_PARAM_NAME);
        if (command != null) {
            ConnectionPool connectionPool = ConnectionPool.getInstance();
            CommandResult commandResult;
            try (ProxyConnection proxyConnection = connectionPool.getConnection()) {
                DaoFactory daoFactory = new DaoFactory(proxyConnection);
                CommandFactory commandFactory = new CommandFactory(daoFactory);
                Command action = commandFactory.create(command);
                commandResult = action.execute(request, response);
                if (commandResult != null) {
                    manageCommandResult(commandResult, request, response);
                } else {
                    logger.error("command is null");
                    dispatch(request, response, "/WEB-INF/web-page/error.jsp");
                }
            } catch (Exception e) {
                logger.error(e);
                dispatch(request, response, "/WEB-INF/web-page/error.jsp");
            }
        } else {
            dispatch(request, response, "/web-page/login.jsp");
        }
    }

    private void dispatch(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher(page);
        dispatcher.forward(request, response);
    }

    private void manageCommandResult(CommandResult commandResult, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page = commandResult.getPage();
        if (commandResult.isForward()) {
            dispatch(request, response, page);
        } else {
            response.sendRedirect(page);
        }
    }
}
