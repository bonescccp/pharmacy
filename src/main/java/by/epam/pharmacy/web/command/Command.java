package by.epam.pharmacy.web.command;

import by.epam.pharmacy.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * interface for commands with method execute
 */
public interface Command {
    CommandResult execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServiceException;
}
