package by.epam.pharmacy.dao.connection;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * Test class for config factory
 */
public class ConfigFactoryTest {
    private static final String FILE_PATH = "src/test/resources/test_database_config.properties";
    private static final String INVALID_FILE_PATH = "src/test/asdes/test_database_config.properties";


    @Test
    public void getDatabaseConfigShouldReturnDataBaseConfigObjectForValidFilePath() throws IOException {
        //given
        ConfigFactory configFactory = new ConfigFactory(FILE_PATH);
        DataBaseConfig expected = new DataBaseConfig("jdbc:mysql://localhost:3306/pharmacy?useTimezone=true&serverTimezone=UTC", "root",
                "root", 8);
        //when
        DataBaseConfig actual = configFactory.getDatabaseConfig();
        //then
        assertEquals(expected, actual);
    }

    @Test(expected = IOException.class)
    public void getDatabaseConfigShouldReturnDataBaseConfigObjectForInValidFilePath() throws IOException {
        //given
        ConfigFactory configFactory = new ConfigFactory(INVALID_FILE_PATH);
        DataBaseConfig expected = new DataBaseConfig("jdbc:mysql://localhost:3306/pharmacy?useTimezone=true&serverTimezone=UTC", "root",
                "root", 8);
        //when
        DataBaseConfig actual = configFactory.getDatabaseConfig();
        //then
        assertEquals(expected, actual);
    }
}
