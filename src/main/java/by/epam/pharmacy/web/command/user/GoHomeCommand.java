package by.epam.pharmacy.web.command.user;

import by.epam.pharmacy.entity.User;
import by.epam.pharmacy.entity.entity_help.UserRole;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Command for going to home page according to user role
 */
public class GoHomeCommand implements Command {
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        CommandResult commandResult;
        HttpSession session = request.getSession(false);

        if (session == null) {
            return new CommandResult("/login.jsp", true);
        } else {
            User user = (User) session.getAttribute("user");
            if (user == null) {
                return new CommandResult("/login.jsp", true);
            } else {
                UserRole userRole = user.getRole();
                commandResult = goHome(userRole);
            }
        }
        return commandResult;
    }

    private CommandResult goHome(UserRole userRole) {
        switch (userRole) {
            case ADMIN:
                return new CommandResult("controller?command=show_users", false);
            case DOCTOR:
                return new CommandResult("controller?command=show_requests", false);
            case PHARMACIST:
                return new CommandResult("controller?command=show_medicines", false);
            case USER:
                return new CommandResult("controller?command=available_medicine", false);
        }
        return null;
    }


}
