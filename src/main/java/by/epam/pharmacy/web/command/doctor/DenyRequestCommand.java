package by.epam.pharmacy.web.command.doctor;

import by.epam.pharmacy.service.RecipeService;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command for not extending recipe according to request
 */
public class DenyRequestCommand implements Command {
    private ServiceFactory serviceFactory;

    public DenyRequestCommand(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String requestId = request.getParameter("request");
        int id = Integer.parseInt(requestId);
        RecipeService recipeService = serviceFactory.createRecipeService();
        recipeService.denyRequest(id);
        String referer = request.getHeader("Referer");
        return new CommandResult(referer, false);
    }
}
