package by.epam.pharmacy.entity.builder;

/**
 * produce builder for entity
 */
public class BuilderFactory {

    public static Builder getBuilder(String table) {
        switch (table) {
            case "medicine":
                return new MedicineBuilder();
            case "user_order":
                return new OrderBuilder();
            case "recipe":
                return new RecipeBuilder();
            case "recipe_request":
                return new RecipeRequestBuilder();
            case "user":
                return new UserBuilder();
            default:
                throw new RuntimeException("Incorrect table name");
        }
    }
}