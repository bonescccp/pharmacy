package by.epam.pharmacy.web.command;

import by.epam.pharmacy.dao.DaoFactory;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.web.command.admin.DeleteUserCommand;
import by.epam.pharmacy.web.command.admin.ShowUsersCommand;
import by.epam.pharmacy.web.command.customer.*;
import by.epam.pharmacy.web.command.doctor.*;
import by.epam.pharmacy.web.command.pharmacist.AddMedicineCommand;
import by.epam.pharmacy.web.command.pharmacist.DeleteMedicineCommand;
import by.epam.pharmacy.web.command.pharmacist.ShowMedicinesCommand;
import by.epam.pharmacy.web.command.user.*;

/**
 * factory for command creation
 */
public class CommandFactory {
    private DaoFactory daoFactory;

    public CommandFactory(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    public Command create(String command) {
        switch (command) {
            case "accept_request":
                return new AcceptRequestCommand(new ServiceFactory(daoFactory));
            case "add_medicine":
                return new AddMedicineCommand(new ServiceFactory(daoFactory));
            case "change_order":
                return new ChangeOrderCommand(new ServiceFactory(daoFactory));
            case "delete_medicine":
                return new DeleteMedicineCommand(new ServiceFactory(daoFactory));
            case "delete_order":
                return new DeleteOrderCommand(new ServiceFactory(daoFactory));
            case "delete_user":
                return new DeleteUserCommand(new ServiceFactory(daoFactory));
            case "deny_request":
                return new DenyRequestCommand(new ServiceFactory(daoFactory));
            case "fill_balance":
                return new FillBalanceCommand(new ServiceFactory(daoFactory));
            case "give_recipe":
                return new GiveRecipeCommand(new ServiceFactory(daoFactory));
            case "go_home":
                return new GoHomeCommand();
            case "localeEN":
                return new ChangeLocaleEnCommand();
            case "localeRU":
                return new ChangeLocaleRuCommand();
            case "login":
                return new LoginCommand(new ServiceFactory(daoFactory));
            case "logout":
                return new LogoutCommand();
            case "order":
                return new OrderCommand(new ServiceFactory(daoFactory));
            case "pay_order":
                return new PayOrderCommand(new ServiceFactory(daoFactory));
            case "request_recipe":
                return new RequestRecipeCommand(new ServiceFactory(daoFactory));
            case "available_medicine":
                return new ShowAvailableMedicines(new ServiceFactory(daoFactory));
            case "show_customers":
                return new ShowCustomersCommand(new ServiceFactory(daoFactory));
            case "show_medicines":
                return new ShowMedicinesCommand(new ServiceFactory(daoFactory));
            case "show_orders":
                return new ShowOrdersCommand(new ServiceFactory(daoFactory));
            case "show_archive_orders":
                return new ShowArchiveOrdersCommand(new ServiceFactory(daoFactory));
            case "show_requests":
                return new ShowRecipeRequestsCommand(new ServiceFactory(daoFactory));
            case "show_all_recipes":
                return new ShowAllRecipesCommand(new ServiceFactory(daoFactory));
            case "show_recipes":
                return new ShowRecipesCommand(new ServiceFactory(daoFactory));
            case "show_users":
                return new ShowUsersCommand(new ServiceFactory(daoFactory));
            default:
                throw new IllegalArgumentException("Unknown command");
        }
    }
}