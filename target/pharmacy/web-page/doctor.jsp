<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="/web-page/header.jsp"%>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="doctor"/>
<html>
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/pages.css">
   <title><fmt:message key="label.header" /></title>
</head>
<body>
<jsp:useBean id="now" class="java.util.Date"/>
<div class="content">
<form class = "form" method = "post" action= "${pageContext.request.contextPath}/controller?command=give_recipe">
<div class="form-inner">
<h2><fmt:message key="label.form"/><br>
    <fmt:message key="label.form2"/></h2>
    <div class="form-content">
    <h3><fmt:message key="label.polz"/></h3>
          <p>
<select size="0" name="user_id" required>
       <option disabled><fmt:message key="label.user" /></option>
        <c:forEach var="num" items="${users}">
       <option value="${num.id}">${num.firstName} ${num.lastName}</option>
       </c:forEach>
      </select>
      </p>
      <h3><fmt:message key="label.med"/></h3>
                <p>
<select size="0" name="medicine_id" required>
       <option disabled><fmt:message key="label.medicine" /></option>
        <c:forEach var="num" items="${medicines}">
       <option value="${num.id}">${num.title} ${num.form.title}</option>
       </c:forEach>
      </select>
      </p>
      <h3><fmt:message key="label.before"/></h3>
                <p>
   <input type="date" name="date" required value = "${now}" min = <fmt:formatDate value="${now}" pattern="yyyy-MM-dd " />>
   </p>
   <p><input type="submit" value=<fmt:message key="label.recipe" /></p>
    </div>
    </div>
</form>
</div>
<div class="category-wrap">
<h3><fmt:message key="label.menu" /></h3>
<ul>
<li><a href="${pageContext.request.contextPath}/controller?command=show_requests"><fmt:message key="label.home" /></a>
<li><a href="${pageContext.request.contextPath}/controller?command=show_all_recipes"><fmt:message key="label.srecipes" /></a>
<li><a href="${pageContext.request.contextPath}/controller?command=show_customers"><fmt:message key="label.recipe" /></a>
<li><a href="${pageContext.request.contextPath}/controller?command=logout"><fmt:message key="label.logout" /></a>
</ul>
</div>
</body>
</html>