drop database if  exists pharmacy;
create database pharmacy;
use pharmacy;

create table user (
                      user_id       int           not null auto_increment,
                      user_login    varchar(12)   not null,
                      user_password varchar(32)   not null,
                      user_role     enum ("admin", "user", "doctor", "pharmacist"),
                      first_name    varchar(10),
                      last_name     varchar(10),
                      money         decimal(6, 2) not null default 0,
                      blocked       boolean       not null default true,
                      primary key (user_id),
                      unique (user_login)
);

create table medicine (
                          medicine_id    int           not null auto_increment,
                          medicine_title varchar(16)   not null,
                          price          decimal(6, 2) not null default 0 CHECK ( price > 0 ),
                          recipe         boolean                default true,
                          form           enum ("pill", "capsule", "injection", "syrup"),
                          is_available   boolean       not null default false,
                          primary key (medicine_id),
                          unique (medicine_title)
);



create table recipe (
                        recipe_id   int     not null auto_increment,
                        user_id     int     not null,
                        medicine_id int     not null,
                        expire_date date,
                        requested   boolean not null default false,
                        primary key (recipe_id)
);

create table recipe_request (
                                request_id int     not null auto_increment,
                                recipe_id  int,
                                date_for   date,
                                seen       boolean not null default false,
                                denied     boolean not null default false,
                                primary key (request_id),
                                unique (recipe_id)
);

create table user_order (
                            order_id    int     not null auto_increment,
                            user_id     int,
                            medicine_id int,
                            quantity    int     not null default 0 CHECK ( quantity > 0 ),
                            payed       boolean not null default false,
                            primary key (order_id)
);


alter table recipe_request 
add foreign key(recipe_id) references recipe(recipe_id);

alter table recipe				
add foreign key (user_id) references user(user_id),																					
add foreign key (medicine_id) references medicine(medicine_id);

alter table user_order																									
add foreign key (user_id) references user(user_id),
add foreign key (medicine_id) references medicine(medicine_id);

