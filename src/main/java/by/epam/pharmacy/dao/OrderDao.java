package by.epam.pharmacy.dao;

import by.epam.pharmacy.entity.Identifiable;
import by.epam.pharmacy.entity.Order;

import java.math.BigDecimal;
import java.util.List;

/**
 * DAO interface for orders
 */
public interface OrderDao<T extends Identifiable> extends Dao<T> {
    /**
     * saves order in database
     *
     * @param userId   id of user
     * @param medId    id of medicine
     * @param quantity quantity of medicine
     * @throws DaoException when something wrong with executing query
     */
    void order(int userId, String medId, String quantity) throws DaoException;

    /**
     * returns orders of some user (by users id)
     *
     * @param userId id of user
     * @return list of active user orders
     * @throws DaoException when something wrong with executing query
     */
    List<Order> getUsersOrders(int userId) throws DaoException;

    /**
     * returns archive orders of some user (by users id)
     *
     * @param userId id of user
     * @return list of archive user orders
     * @throws DaoException when something wrong with executing query
     */
    List<Order> getArchiveUsersOrders(int userId) throws DaoException;

    /**
     * deletes order (by id)
     *
     * @param orderId id of order
     * @throws DaoException when something wrong with executing query
     */
    void deleteOrder(String orderId) throws DaoException;

    /**
     * changes quantity
     *
     * @param orderId  id of order
     * @param quantity new quantity
     * @throws DaoException when something wrong with executing query
     */
    void changeOrder(String orderId, String quantity) throws DaoException;

    /**
     * make order completed
     *
     * @param orderId id of order
     * @param cost    sum of money
     * @param userId  id of user
     * @throws DaoException when something wrong with executing query
     */
    void payOrder(String orderId, BigDecimal cost, int userId) throws DaoException;
}
