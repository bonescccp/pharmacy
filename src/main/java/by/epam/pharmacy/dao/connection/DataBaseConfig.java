package by.epam.pharmacy.dao.connection;

import java.util.Objects;

/**
 * class for storing params of data base connection
 */
public class DataBaseConfig {
    private final String url;
    private final String user;
    private final String password;
    private final int poolSize;


    public DataBaseConfig(String url, String user, String password, int poolSize) {
        this.url = url;
        this.user = user;
        this.password = password;
        this.poolSize = poolSize;
    }

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public int getPoolSize() {
        return poolSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataBaseConfig that = (DataBaseConfig) o;
        return poolSize == that.poolSize &&
                Objects.equals(url, that.url) &&
                Objects.equals(user, that.user) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url, user, password, poolSize);
    }
}
