package by.epam.pharmacy.entity.builder;

import by.epam.pharmacy.entity.Recipe;
import by.epam.pharmacy.entity.RecipeRequest;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Builder class for recipe request
 */
public class RecipeRequestBuilder implements Builder<RecipeRequest> {
    private static final String ID = "request_id";
    private static final String DATE_FOR = "date_for";
    private static final String SEEN = "seen";
    private static final String DENIED = "denied";

    private Builder recipeBuilder;

    public RecipeRequestBuilder() {
        recipeBuilder = new RecipeBuilder();
    }

    @Override
    public RecipeRequest build(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt(ID);
        Date date = resultSet.getDate(DATE_FOR);
        int seen = resultSet.getInt(SEEN);
        int denied = resultSet.getInt(DENIED);

        boolean isSeen = seen == 1;
        boolean isDenied = denied == 1;
        Recipe recipe = (Recipe) recipeBuilder.build(resultSet);
        return new RecipeRequest(id, recipe, date, isSeen, isDenied);
    }
}
