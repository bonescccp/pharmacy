package by.epam.pharmacy.web.command.user;

import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command for logging out
 */
public class LogoutCommand implements Command {

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) {
        request.getSession().invalidate();
        request.setAttribute("error", "You logged out. Good bye!");
        return new CommandResult("login.jsp", true);
    }
}
