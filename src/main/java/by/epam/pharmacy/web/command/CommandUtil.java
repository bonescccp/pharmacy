package by.epam.pharmacy.web.command;

import by.epam.pharmacy.entity.Medicine;
import by.epam.pharmacy.entity.Recipe;
import by.epam.pharmacy.service.MedicineService;
import by.epam.pharmacy.service.RecipeService;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * class for servicing command (getting list of available medicines)
 */
public class CommandUtil {
    private ServiceFactory serviceFactory;

    public CommandUtil(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    public List<Medicine> getAvailableMedicine(int userId) throws ServiceException {
        MedicineService medicineService = serviceFactory.createMedicineService();
        RecipeService recipeService = serviceFactory.createRecipeService();
        List<Recipe> recipes = recipeService.getRecipes(userId);
        List<Medicine> medicines = medicineService.getMedicines();
        List<Medicine> availableMedicines = new ArrayList<>();
        for (Medicine medicine : medicines) {
            if (!medicine.isRecipe() && medicine.isAvailable()) {
                availableMedicines.add(medicine);
            } else {
                String medTitle = medicine.getTitle();
                for (Recipe recipe : recipes) {
                    if (recipe.isValid()) {
                        String title = recipe.getMedicine().getTitle();
                        if (medTitle.equals(title)) {
                            availableMedicines.add(medicine);
                        }
                    }
                }
            }
        }
        return availableMedicines;
    }
}
