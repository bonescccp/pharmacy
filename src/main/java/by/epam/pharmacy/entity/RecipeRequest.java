package by.epam.pharmacy.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Recipe request class
 */
public class RecipeRequest implements Identifiable, Serializable {

    private static final long serialVersionUID = 1L;

    private final int id;
    private final Recipe recipe;
    private final Date date;
    private final boolean seen;
    private final boolean denied;

    public RecipeRequest(int id, Recipe recipe, Date date, boolean seen, boolean denied) {
        this.id = id;
        this.recipe = recipe;
        this.date = date;
        this.seen = seen;
        this.denied = denied;
    }

    public int getId() {
        return id;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public Date getDate() {
        return date;
    }

    public boolean isSeen() {
        return seen;
    }

    public boolean isDenied() {
        return denied;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RecipeRequest that = (RecipeRequest) o;
        return id == that.id &&
                seen == that.seen &&
                denied == that.denied &&
                Objects.equals(recipe, that.recipe) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, recipe, date, seen, denied);
    }
}
