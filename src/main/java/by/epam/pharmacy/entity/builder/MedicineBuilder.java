package by.epam.pharmacy.entity.builder;

import by.epam.pharmacy.entity.Medicine;
import by.epam.pharmacy.entity.entity_help.MedicineForm;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Builder class for medicine
 */
public class MedicineBuilder implements Builder<Medicine> {
    private static final String ID = "medicine_id";
    private static final String TITLE = "medicine_title";
    private static final String PRICE = "price";
    private static final String RECIPE = "recipe";
    private static final String FORM = "form";
    private static final String AVAILABLE = "is_available";

    @Override
    public Medicine build(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt(ID);
        String title = resultSet.getString(TITLE);
        BigDecimal price = resultSet.getBigDecimal(PRICE);
        String form = resultSet.getString(FORM);
        int recipe = resultSet.getInt(RECIPE);
        int availability = resultSet.getInt(AVAILABLE);

        MedicineForm medicineForm = MedicineForm.valueOf(form.toUpperCase());
        boolean byRecipe = recipe == 1;
        boolean available = availability == 1;
        return new Medicine(id, title, price, medicineForm, byRecipe, available);
    }
}

