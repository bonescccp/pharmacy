<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="header"/>
<html>
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/header.css">
</head>
<header>
      <div class="topnav">
        <img src = "${pageContext.request.contextPath}/resources/images/crest.png" width="48px" height="48px" align="left" alt = "Epam-Pharm">
        <a href="${pageContext.request.contextPath}/controller?command=localeEN"><fmt:message key="label.en" /></a>
        <a href="${pageContext.request.contextPath}/controller?command=localeRU"><fmt:message key="label.ru" /></a>
        <c:if test="${sessionScope.user != null}">
        <a href="${pageContext.request.contextPath}/controller?command=logout"><fmt:message key="label.logout" /></a>
         </c:if>
      </div>
</header>
</html>