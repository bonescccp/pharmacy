package by.epam.pharmacy.web.command.customer;

import by.epam.pharmacy.entity.Recipe;
import by.epam.pharmacy.service.RecipeService;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;
import by.epam.pharmacy.web.command.Paginator;
import by.epam.pharmacy.web.command.UserIdGetter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Command for getting users recipes
 */
public class ShowRecipesCommand implements Command {
    private ServiceFactory serviceFactory;
    private Paginator<Recipe> paginator;

    public ShowRecipesCommand(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
        paginator = new Paginator<>();
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        int id = UserIdGetter.getUserId(request);
        RecipeService recipeService = serviceFactory.createRecipeService();
        List<Recipe> recipes = recipeService.getRecipes(id);
        int page = 1;
        int recordsPerPage = 5;
        if (request.getParameter("page") != null)
            page = Integer.parseInt(request.getParameter("page"));
        int numberOfRecords = recipes.size();
        List<Recipe> pageList = paginator.paginate(recipes, page, recordsPerPage);
        int numberOfPages = paginator.countPages(numberOfRecords, recordsPerPage);
        request.setAttribute("numberOfPages", numberOfPages);
        request.setAttribute("currentPage", page);
        request.setAttribute("recipes", pageList);
        return new CommandResult("/web-page/recipes.jsp", true);
    }
}





