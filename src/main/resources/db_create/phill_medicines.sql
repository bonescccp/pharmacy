insert into medicine (medicine_title, price, recipe, form, is_available)
values ("aspirine", 15, 1, "pill", 1);
insert into medicine (medicine_title, price, recipe, form, is_available)
values ("analgine", 25, 1, "pill", 1);
insert into medicine (medicine_title, price, recipe, form, is_available)
values ("iod", 12, 0, "injection", 1);
insert into medicine (medicine_title, price, recipe, form, is_available)
values ("zelenka", 8, 0, "injection", 1);
insert into medicine (medicine_title, price, recipe, form, is_available)
values ("nozivin", 12.3, 0, "syrup", 1);
insert into medicine (medicine_title, price, recipe, form, is_available)
values ("capsicam", 15.3, 0, "capsule", 1);
insert into medicine (medicine_title, price, recipe, form, is_available)
values ("terraflu", 10.8, 1, "capsule", 1);
insert into medicine (medicine_title, price, recipe, form, is_available)
values ("gastal", 11, 0, "pill", 1);
insert into medicine (medicine_title, price, recipe, form, is_available)
values ("omeprazol", 11.83, 1, "pill", 1);
insert into medicine (medicine_title, price, recipe, form, is_available)
values ("lazolvan", 111.6, 1, "capsule", 1);
insert into medicine (medicine_title, price, recipe, form, is_available)
values ("heroin", 18, 1, "pill", 1);
insert into medicine (medicine_title, price, recipe, form, is_available)
values ("glicin", 1.5, 0, "pill", 1);
insert into medicine (medicine_title, price, recipe, form, is_available)
values ("glukoza", 0.3, 0, "pill", 1);