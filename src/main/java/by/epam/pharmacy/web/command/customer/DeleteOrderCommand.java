package by.epam.pharmacy.web.command.customer;

import by.epam.pharmacy.service.CustomerService;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command for deleting user order
 */
public class DeleteOrderCommand implements Command {
    private ServiceFactory serviceFactory;

    public DeleteOrderCommand(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String orderId = request.getParameter("order_id");
        CustomerService customerService = serviceFactory.createCustomerService();
        customerService.deleteOrder(orderId);
        String referer = request.getHeader("Referer");
        return new CommandResult(referer, false);
    }
}
