package by.epam.pharmacy.dao.impl;

import by.epam.pharmacy.dao.AbstractDao;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.MedicineDao;
import by.epam.pharmacy.dao.connection.ProxyConnection;
import by.epam.pharmacy.entity.Medicine;
import by.epam.pharmacy.entity.MedicineParams;
import by.epam.pharmacy.entity.builder.BuilderFactory;

import java.util.List;
import java.util.Optional;


/**
 * implementation of DAO class for medicines
 */

public class MedicineDaoImpl extends AbstractDao<Medicine> implements MedicineDao<Medicine> {
    private static final String FIND_BY_ID =
            "select * from medicine where medicine_id = ?";
    private static final String ADD_MEDICINE = "insert into medicine (medicine_title, price, " +
            "recipe, form, is_available) values (?, ?, ?, ?, ?)";
    private static final String DELETE_MEDICINE_BY_ID = "update medicine set is_available " +
            " = ? where medicine_id = ?";
    private static final String GET_RECIPED_MEDICINES =
            "select * from medicine where recipe = true and is_available = true";

    public MedicineDaoImpl(ProxyConnection proxyConnection) {
        super(proxyConnection);
    }

    @Override
    public Optional<Medicine> getById(int id) throws DaoException {
        return executeForSingleResult(
                FIND_BY_ID,
                BuilderFactory.getBuilder("medicine"),
                Integer.toString(id)
        );
    }

    /**
     * @see MedicineDao#arcivateById(int)
     */
    @Override
    public void arcivateById(int id) throws DaoException {
        executeUpdate(DELETE_MEDICINE_BY_ID, "0", Integer.toString(id));
    }

    /**
     * @see MedicineDao#unZipById(int)
     */
    @Override
    public void unZipById(int id) throws DaoException {
        executeUpdate(DELETE_MEDICINE_BY_ID, "1", Integer.toString(id));
    }

    /**
     * @see MedicineDao#saveMedicine(MedicineParams)
     */
    @Override
    public void saveMedicine(MedicineParams entity) throws DaoException {
        executeUpdate(ADD_MEDICINE, entity.getTitle(), entity.getPrice(), entity.getRecipe(), entity.getForm(), entity.getAvailable());
    }

    @Override
    protected String getTableName() {
        return "medicine";
    }

    /**
     * @see MedicineDao#getRecipedMedicines()
     */
    @Override
    public List<Medicine> getRecipedMedicines() throws DaoException {
        return executeQuery(GET_RECIPED_MEDICINES, BuilderFactory.getBuilder("medicine"));
    }
}
