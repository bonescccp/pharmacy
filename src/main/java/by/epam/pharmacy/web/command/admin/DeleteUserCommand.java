package by.epam.pharmacy.web.command.admin;

import by.epam.pharmacy.service.AdminService;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command that block user if it is active and unblock if it is blocked
 */
public class DeleteUserCommand implements Command {
    private ServiceFactory serviceFactory;

    public DeleteUserCommand(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String stringId = request.getParameter("delete");
        int id = Integer.parseInt(stringId);
        AdminService adminService = serviceFactory.createAdminService();
        adminService.deleteUserById(id);
        String referer = request.getHeader("Referer");
        return new CommandResult(referer, false);
    }
}


