package by.epam.pharmacy.service;

import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.DaoFactory;
import by.epam.pharmacy.dao.OrderDao;
import by.epam.pharmacy.dao.UserDao;
import by.epam.pharmacy.entity.Medicine;
import by.epam.pharmacy.entity.Order;
import by.epam.pharmacy.entity.User;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * Service for customers (users with role user)
 */
public class CustomerService implements Service {
    private DaoFactory daoFactory;

    public CustomerService(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    public List<User> getCustomers() throws ServiceException {
        UserDao userDao = daoFactory.createUserDaoImpl();
        try {
            return userDao.getCustomers();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public Optional<User> getUserById(int id) throws ServiceException {
        try {
            UserDao userDao = daoFactory.createUserDaoImpl();
            Optional<User> user = userDao.getById(id);
            return user;
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    /**
     * creates new order in users cabinet
     *
     * @param userId     id of user
     * @param medicineId id of ordered medicine
     * @param quantity   quantity of ordered medicine
     * @throws ServiceException when error with DAO
     */
    public void order(int userId, String medicineId, String quantity) throws ServiceException {
        OrderDao orderDao = daoFactory.createOrderDaoImpl();
        try {
            orderDao.order(userId, medicineId, quantity);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    /**
     * deletes order in users cabinet
     *
     * @param orderId id of order you want to delete
     * @throws ServiceException when error with DAO
     */
    public void deleteOrder(String orderId) throws ServiceException {
        OrderDao orderDao = daoFactory.createOrderDaoImpl();
        try {
            orderDao.deleteOrder(orderId);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public void changeOrder(String orderId, String quantity) throws ServiceException {
        OrderDao orderDao = daoFactory.createOrderDaoImpl();
        try {
            orderDao.changeOrder(orderId, quantity);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public List<Order> getUsersOrders(int userId) throws ServiceException {
        OrderDao orderDao = daoFactory.createOrderDaoImpl();
        try {
            return orderDao.getUsersOrders(userId);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public List<Order> getArchiveUsersOrders(int userId) throws ServiceException {
        OrderDao orderDao = daoFactory.createOrderDaoImpl();
        try {
            return orderDao.getArchiveUsersOrders(userId);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    /**
     * adds money to users bill
     *
     * @param id     id of user
     * @param amount quantity of money medicine
     * @throws ServiceException when error with DAO
     */
    public void pay(String amount, int id) throws ServiceException {
        UserDao userDao = daoFactory.createUserDaoImpl();
        try {
            userDao.addMoney(id, amount);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public Optional<Order> getById(int id) throws ServiceException {
        OrderDao orderDao = daoFactory.createOrderDaoImpl();
        Optional<Order> order = null;
        try {
            order = orderDao.getById((id));
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return order;
    }

    /**
     * withdraw money from bill and complete users order
     *
     * @param orderId id of user
     * @param userId  id of ordered medicine
     * @throws ServiceException when error with DAO
     */
    public void payOrder(String orderId, int userId) throws ServiceException {
        OrderDao orderDao = daoFactory.createOrderDaoImpl();
        Order order;
        Medicine medicine;
        BigDecimal cost;
        BigDecimal totalCost;
        try {
            order = (Order) orderDao.getById(Integer.parseInt(orderId)).get();
            medicine = order.getMedicine();
            int quantity = order.getQuantity();
            BigDecimal price = medicine.getPrice();
            cost = price.multiply(new BigDecimal(quantity));
            totalCost = cost.setScale(2);
            orderDao.payOrder(orderId, totalCost, userId);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
