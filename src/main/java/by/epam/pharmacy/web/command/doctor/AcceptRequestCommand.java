package by.epam.pharmacy.web.command.doctor;

import by.epam.pharmacy.service.RecipeService;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command for extending recipe according to request
 */
public class AcceptRequestCommand implements Command {
    private ServiceFactory serviceFactory;

    public AcceptRequestCommand(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String requestId = request.getParameter("request");
        int id = Integer.parseInt(requestId);
        RecipeService recipeService = serviceFactory.createRecipeService();
        recipeService.acceptRequest(id);
        return new CommandResult("controller?command=show_requests", false);
    }
}
