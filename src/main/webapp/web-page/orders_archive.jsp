<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="/WEB-INF/sumCountTag.tld" prefix="epam" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/web-page/header.jsp" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="user"/>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/pages.css">
    <title><fmt:message key="label.archive"/></title>
</head>
<body>
<div class="content">
    <table class="table">
        <c:if test="${fn:length(orders) gt 0}">
            <tr>
                <th><fmt:message key="label.title"/></th>
                <th><fmt:message key="label.quantity"/></th>
            </tr>

            <c:forEach var="order" items="${orders}">
                <tr>
                    <td>${order.medicine.title}</td>
                    <td>${order.quantity}</td>
                    <td><epam:sum price="${order.medicine.price}" quantity="${order.quantity}"/></td>
                </tr>
            </c:forEach>
            <tr>
            <td class="round-bottom" colspan = "2" >
            <c:if test="${numberOfPages gt 1}">
                <c:if test="${currentPage != 1}">
                    <a href="controller?command=show_archive_orders&page=${currentPage - 1}">←</a>
                </c:if>
                <c:forEach begin="1" end="${numberOfPages}" var="i">
                    <c:choose>
                        <c:when test="${currentPage eq i}">
                            ${i}
                        </c:when>
                        <c:otherwise>
                            <a href="controller?command=show_archive_orders&page=${i}">${i}</a>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                <c:if test="${currentPage lt numberOfPages}">
                    <td class="table"><a href="controller?command=show_archive_orders&page=${currentPage + 1}">→</a>
                    </td>
                </c:if>
                </tr>
            </c:if>
        </c:if>
        <c:if test="${fn:length(orders) == 0}">
        <tr>
            <td class="round-bottom" colspan="2">
                    <fmt:message key="label.emptyordersarchive"/>
                </c:if>
        </tr>
    </table>
</div>
<div class="category-wrap">
    <h3><fmt:message key="label.menu"/></h3>
    <ul>
        <li><a href="${pageContext.request.contextPath}/web-page/fill_balance.jsp"><fmt:message key="label.balance"/>
            (${sessionScope.user.money})</a>
        <li><a href="${pageContext.request.contextPath}/controller?command=available_medicine"><fmt:message
                key="label.home"/></a>
        <li><a href="${pageContext.request.contextPath}/controller?command=show_orders"><fmt:message
                key="label.orders"/></a>
        <li><a href="${pageContext.request.contextPath}/controller?command=show_archive_orders"><fmt:message
                key="label.archive"/></a>
        <li><a href="${pageContext.request.contextPath}/controller?command=show_recipes"><fmt:message
                key="label.recipes"/></a>
        <li><a href="${pageContext.request.contextPath}/controller?command=logout"><fmt:message key="label.logout"/></a>
    </ul>
</div>
</body>
</html>