package by.epam.pharmacy.service.validator;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test class for data validator
 */
public class ValidatorTest {
    public static final String VALID_QUANTITY = "3";
    public static final String INVALID_QUANTITY = "-3";
    public static final String VALID_DATE = "2021-12-12";
    public static final String PAST_DATE = "2018-12-12";


    private static final Validator VALIDATOR = new Validator();

    @Test
    public void validateQuantityShouldReturnTrueForValidQuantity() {
        //given
        //when
        boolean actual = VALIDATOR.validateQuantity(VALID_QUANTITY);
        //then
        assertTrue(actual);
    }

    @Test
    public void validateQuantityShouldReturnFalseForInValidQuantity() {
        //given
        //when
        boolean actual = VALIDATOR.validateQuantity(INVALID_QUANTITY);
        //then
        assertFalse(actual);
    }

    @Test
    public void validateMoneyShouldReturnTrueForValidQuantity() {
        //given
        //when
        boolean actual = VALIDATOR.validateMoney(VALID_QUANTITY);
        //then
        assertTrue(actual);
    }

    @Test
    public void validateMoneyShouldReturnFalseForInValidQuantity() {
        //given
        //when
        boolean actual = VALIDATOR.validateMoney(INVALID_QUANTITY);
        //then
        assertFalse(actual);
    }

    @Test
    public void validateDateShouldReturnTrueForValidDate() {
        //given
        //when
        boolean actual = VALIDATOR.validateDate(VALID_DATE);
        //then
        assertTrue(actual);
    }

    @Test
    public void validateDateShouldReturnFalseForPastDate() {
        //given
        //when
        boolean actual = VALIDATOR.validateDate(PAST_DATE);
        //then
        assertFalse(actual);
    }


}
