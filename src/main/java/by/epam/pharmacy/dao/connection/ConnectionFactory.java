package by.epam.pharmacy.dao.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Produces connections to database
 */
public class ConnectionFactory {
    private static final String DRIVER_NAME = "com.mysql.jdbc.Driver";

    private DataBaseConfig dataBaseConfig;

    /**
     * @param dataBaseConfig configuration of data base
     * @see DataBaseConfig
     */
    public ConnectionFactory(DataBaseConfig dataBaseConfig) {
        this.dataBaseConfig = dataBaseConfig;
        try {
            Class.forName(DRIVER_NAME);
        } catch (ClassNotFoundException e) {
            throw new ConnectionToDataBaseException("DB driver not found!", e);
        }
    }

    public Connection getConnection() {
        String url = dataBaseConfig.getUrl();
        String user = dataBaseConfig.getUser();
        String password = dataBaseConfig.getPassword();

        try {
            return DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            throw new ConnectionToDataBaseException("Failed get connection to DB!", e);
        }
    }
}

