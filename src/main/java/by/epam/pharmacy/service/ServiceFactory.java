package by.epam.pharmacy.service;

import by.epam.pharmacy.dao.DaoFactory;

/**
 * Factory for producing services
 */
public class ServiceFactory {
    private DaoFactory daoFactory;

    public ServiceFactory(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    public UserService createUserService() {
        return new UserService(daoFactory);
    }

    public CustomerService createCustomerService() {
        return new CustomerService(daoFactory);
    }

    public AdminService createAdminService() {
        return new AdminService(daoFactory);
    }

    public MedicineService createMedicineService() {
        return new MedicineService(daoFactory);
    }

    public RecipeService createRecipeService() {
        return new RecipeService(daoFactory);
    }
}
