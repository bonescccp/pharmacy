<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/login_form.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/pages.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/header.css">
    <fmt:setLocale value="${param.lang}"/>
    <fmt:setBundle basename="login"/>
    <title><fmt:message key="label.header"/></title>
</head>
<header>
    <div class="topnav">
        <img src="${pageContext.request.contextPath}/resources/images/crest.png" width="48px" height="48px" align="left"
             alt="Epam-Pharm">
        <a href="${pageContext.request.contextPath}/?lang=en"><fmt:message key="label.en"/></a>
        <a href="${pageContext.request.contextPath}/?lang=ru"><fmt:message key="label.ru"/></a>
    </div>
</header>
<body>
<form class="form-3" method="post" action="${pageContext.request.contextPath}/controller?command=login">
    <p class="clearfix">
        <label for="login"><fmt:message key="label.login"/></label>
        <input type="text" name="login" id="login" placeholder=
        <fmt:message key="label.login"/> required pattern="[A-Za-z]{3,6}[0-9A-Za-z]{0,6}">
    </p>
    <p class="clearfix">
        <label for="password"><fmt:message key="label.password"/></label>
        <input type="password" name="password" id="password" placeholder=
        <fmt:message key="label.password"/> required required pattern="[0-9A-Za-z]{4,12}">
    <p class="clearfix">
        <button>
            <div><fmt:message key="label.button"/></div>
        </button>
    </p>
    <p style="color: white;font-size: 55%; text-align: right"> ${error}</p>
</form>
</body>
</html>