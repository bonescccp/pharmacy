package by.epam.pharmacy.web.command;

import java.util.List;

public class Paginator<T> {
    public int countPages(int numberOfRecords, int recordsPerPage) {
        int numberOfPages = (int) Math.ceil(numberOfRecords * 1.0 / recordsPerPage);
        return numberOfPages;
    }

    public List<T> paginate(List<T> data, int page, int recordsPerPage) {
        List<T> pageList;
        int numberOfRecords = data.size();
        if (numberOfRecords < recordsPerPage) {
            pageList = data;
        } else {
            int endNum = ((page - 1) * recordsPerPage + recordsPerPage);
            if (endNum > numberOfRecords) {
                endNum = numberOfRecords;
            }
            pageList = data.subList((page - 1) * recordsPerPage, endNum);
        }
        return pageList;
    }
}
