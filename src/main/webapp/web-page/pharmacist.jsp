<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/web-page/header.jsp" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="pharmacist"/>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/pages.css">
    <title><fmt:message key="label.header"/></title>
</head>
<body>
<div class="content">
    <table class="table">
        <c:if test="${fn:length(medicines) gt 0}">
            <tr>
                <th><fmt:message key="label.title"/></th>
                <th><fmt:message key="label.price"/></th>
                <th><fmt:message key="label.form"/></th>
                <th><fmt:message key="label.recipef"/></th>
                <th><fmt:message key="label.available"/></th>
                <th><fmt:message key="label.archive"/></th>
            </tr>

            <c:forEach var="medicine" items="${medicines}">
                <tr>
                    <td>${medicine.title}</td>
                    <td>${medicine.price} $</td>
                    <td>
                        <c:choose>
                            <c:when test="${medicine.form == 'PILL'}">
                                <fmt:message key="label.pill"/>
                            </c:when>
                            <c:when test="${medicine.form == 'CAPSULE'}">
                                <fmt:message key="label.capsule"/>
                            </c:when>
                            <c:when test="${medicine.form == 'INJECTION'}">
                                <fmt:message key="label.injection"/>
                            </c:when>
                            <c:when test="${medicine.form == 'SYRUP'}">
                                <fmt:message key="label.syrup"/>
                            </c:when>
                        </c:choose>
                    </td>
                    <td>
                        <c:if test="${medicine.recipe == true}">
                            ✓
                        </c:if>
                        <c:if test="${medicine.recipe == false}">
                            &nbsp
                        </c:if>
                    </td>
                    <td>
                        <c:if test="${medicine.available == true}">
                            ✓
                        </c:if>
                        <c:if test="${medicine.available == false}">
                            &nbsp
                        </c:if>
                    </td>
                    <c:if test="${medicine.available == true}">
                        <td>
                            <form onSubmit='return confirm("<fmt:message key="label.delete"/> ${medicine.title} ?");' method="post"
                                  action="${pageContext.request.contextPath}/controller?command=delete_medicine">
                                <button type="submit" name="delete" value="${medicine.id}"><fmt:message
                                        key="label.delete"/></button>
                            </form>
                        </td>
                    </c:if>
                    <c:if test="${medicine.available == false}">
                        <td>
                            <form onSubmit='return confirm("<fmt:message key="label.resume"/> ${medicine.title} ?");' method="post"
                                  action="${pageContext.request.contextPath}/controller?command=delete_medicine">
                                <button type="submit" name="delete" value="${medicine.id}"><fmt:message
                                        key="label.resume"/></button>
                            </form>
                        </td>
                    </c:if>
                </tr>
            </c:forEach>
            <tr>
            <td class="round-bottom" colspan = "6" >
            <c:if test="${numberOfPages gt 1}">
                <c:if test="${currentPage != 1}">
                    <a href="controller?command=show_medicines&page=${currentPage - 1}">←</a>
                </c:if>
                <c:forEach begin="1" end="${numberOfPages}" var="i">
                    <c:choose>
                        <c:when test="${currentPage eq i}">
                            ${i}
                        </c:when>
                        <c:otherwise>
                            <a href="controller?command=show_medicines&page=${i}">${i}</a>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                <c:if test="${currentPage lt numberOfPages}">
                    <a href="controller?command=show_medicines&page=${currentPage + 1}">→</a>
                </c:if>
                </td>
                </tr>
            </c:if>
        </c:if>
        <c:if test="${fn:length(medicines) == 0}">
        <tr>
            <td class="round-bottom" colspan="6">
                    <fmt:message key="label.emptymedicines"/>
                </c:if>
        </tr>
    </table>

    <form action="web-page/medicine_add.jsp">
        <button type="submit"><fmt:message key="label.add"/></button>
    </form>
</div>
<div class="category-wrap">
    <h3><fmt:message key="label.menu"/></h3>
    <ul>
        <li><a href="${pageContext.request.contextPath}/controller?command=show_medicines"><fmt:message
                key="label.home"/></a>
        <li><a href="web-page/medicine_add.jsp"><fmt:message key="label.add"/></a>
        <li><a href="${pageContext.request.contextPath}/controller?command=logout"><fmt:message key="label.logout"/></a>
    </ul>
</div>
</body>
</html>