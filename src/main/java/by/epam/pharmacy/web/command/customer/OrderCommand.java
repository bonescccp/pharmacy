package by.epam.pharmacy.web.command.customer;

import by.epam.pharmacy.entity.Medicine;
import by.epam.pharmacy.service.CustomerService;
import by.epam.pharmacy.service.MedicineService;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.service.validator.Validator;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;
import by.epam.pharmacy.web.command.CommandUtil;
import by.epam.pharmacy.web.command.UserIdGetter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Command for making order with validation of quantity and avaliability of medicine
 */
public class OrderCommand implements Command {
    private ServiceFactory serviceFactory;
    private Validator validator;

    public OrderCommand(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
        validator = new Validator();
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String medicineId = request.getParameter("med");
        String stringQuantity = request.getParameter("qty");
        int userId = UserIdGetter.getUserId(request);
        CommandUtil commandUtil = new CommandUtil(serviceFactory);
        List<Medicine> availableMedicines = commandUtil.getAvailableMedicine(userId);
        MedicineService medicineService = serviceFactory.createMedicineService();
        Medicine requestedMedicine = medicineService.getById(Integer.parseInt(medicineId)).get();
        if (validator.validateQuantity(stringQuantity) && availableMedicines.contains(requestedMedicine)) {
            CustomerService customerService = serviceFactory.createCustomerService();
            customerService.order(userId, medicineId, stringQuantity);
            return new CommandResult("controller?command=show_orders", false);
        } else {
            throw new IllegalArgumentException("Illegal attempt to order " + requestedMedicine + " quantity :" + stringQuantity);
        }
    }
}
