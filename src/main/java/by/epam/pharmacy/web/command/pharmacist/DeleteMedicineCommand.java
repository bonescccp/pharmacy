package by.epam.pharmacy.web.command.pharmacist;

import by.epam.pharmacy.service.MedicineService;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command for sending medicine to archive
 */
public class DeleteMedicineCommand implements Command {
    private ServiceFactory serviceFactory;

    public DeleteMedicineCommand(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String stringId = request.getParameter("delete");
        int id = Integer.parseInt(stringId);
        MedicineService medicineService = serviceFactory.createMedicineService();
        medicineService.deleteMedicineById(id);
        String referer = request.getHeader("Referer");
        return new CommandResult(referer, false);
    }
}

