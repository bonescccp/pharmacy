package by.epam.pharmacy.dao;

import by.epam.pharmacy.dao.connection.ProxyConnection;
import by.epam.pharmacy.dao.impl.*;

/**
 * class for production DAO implementations
 */
public class DaoFactory {
    private ProxyConnection connection;

    public DaoFactory(ProxyConnection connection) {
        this.connection = connection;
    }

    public UserDaoImpl createUserDaoImpl() {
        return new UserDaoImpl(connection);
    }

    public MedicineDaoImpl createMedicineDaoImpl() {
        return new MedicineDaoImpl(connection);
    }

    public RecipeDaoImpl createRecipeDaoImpl() {
        return new RecipeDaoImpl(connection);
    }

    public RecipeRequestDaoImpl createRecipeRequestDaoImpl() {
        return new RecipeRequestDaoImpl(connection);
    }

    public OrderDao createOrderDaoImpl() {
        return new OrderDaoImpl(connection);
    }
}