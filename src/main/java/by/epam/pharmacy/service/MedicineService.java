package by.epam.pharmacy.service;

import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.DaoFactory;
import by.epam.pharmacy.dao.MedicineDao;
import by.epam.pharmacy.entity.Medicine;
import by.epam.pharmacy.entity.MedicineParams;

import java.util.List;
import java.util.Optional;

/**
 * Service for managing medicines
 */
public class MedicineService implements Service {
    private DaoFactory dao;

    public MedicineService(DaoFactory dao) {
        this.dao = dao;
    }

    public List<Medicine> getMedicines() throws ServiceException {
        MedicineDao medicineDao = dao.createMedicineDaoImpl();
        try {
            return medicineDao.getAll();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    /**
     * @return list of medicines selling by recipe
     * @throws ServiceException when error completing task
     */
    public List<Medicine> getRecipedMedicines() throws ServiceException {
        MedicineDao medicineDao = dao.createMedicineDaoImpl();
        try {
            return medicineDao.getRecipedMedicines();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    /**
     * send medicine to archive if it is active and take from archive if it is in archive
     *
     * @param id id of medicine
     * @throws ServiceException when error completing task
     */
    public void deleteMedicineById(int id) throws ServiceException {
        MedicineDao medicineDao = dao.createMedicineDaoImpl();
        try {
            Optional<Medicine> medicine = medicineDao.getById(id);
            if (medicine.isPresent()) {
                if (medicine.get().isAvailable()) {
                    medicineDao.arcivateById(id);
                } else {
                    medicineDao.unZipById(id);
                }
            }
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public Optional<Medicine> getById(int id) throws ServiceException {
        MedicineDao medicineDao = dao.createMedicineDaoImpl();
        try {
            return medicineDao.getById(id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public void save(MedicineParams medicineParams) throws ServiceException {
        MedicineDao medicineDao = dao.createMedicineDaoImpl();
        try {
            medicineDao.saveMedicine(medicineParams);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
