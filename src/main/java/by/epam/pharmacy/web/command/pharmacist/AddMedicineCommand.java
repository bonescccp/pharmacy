package by.epam.pharmacy.web.command.pharmacist;

import by.epam.pharmacy.entity.Medicine;
import by.epam.pharmacy.entity.MedicineParams;
import by.epam.pharmacy.service.MedicineService;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.service.validator.Validator;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Command for creating new medicine
 */
public class AddMedicineCommand implements Command {
    public static final String TITLE_REGEX = "[A-Za-z]{3,8}[0-9A-Fa-f]{0,8}";

    private ServiceFactory serviceFactory;
    private Validator validator;

    public AddMedicineCommand(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
        validator = new Validator();
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response)
            throws ServiceException {
        String title = request.getParameter("title");
        String price = request.getParameter("price");
        String form = request.getParameter("form");
        String recipe = request.getParameter("recipe");
        String available = request.getParameter("available");

        if (!checkTitle(title)) {
            return new CommandResult("web-page/medicine_add_error.jsp", false);
        }

        if (validator.validateMoney(price) && validator.validateTitle(TITLE_REGEX, title)) {
            MedicineService medicineService = serviceFactory.createMedicineService();
            MedicineParams newMedicine = new MedicineParams(title, price, form, recipe, available);
            medicineService.save(newMedicine);
            return new CommandResult("controller?command=show_medicines", false);
        } else {
            throw new IllegalArgumentException("incorrect data for input medicine : price : " + price + " title : " + title);
        }
    }

    private boolean checkTitle(String title) throws ServiceException {
        MedicineService medicineService = serviceFactory.createMedicineService();
        List<Medicine> medicines = medicineService.getMedicines();
        for (Medicine medicine : medicines) {
            if (medicine.getTitle().equals(title)) {
                return false;
            }
        }
        return true;
    }
}

