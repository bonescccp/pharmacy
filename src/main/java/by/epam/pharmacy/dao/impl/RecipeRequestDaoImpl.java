package by.epam.pharmacy.dao.impl;

import by.epam.pharmacy.dao.AbstractDao;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.RecipeRequestDao;
import by.epam.pharmacy.dao.connection.ProxyConnection;
import by.epam.pharmacy.entity.RecipeRequest;
import by.epam.pharmacy.entity.builder.RecipeRequestBuilder;

import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.List;
import java.util.Optional;

/**
 * implementation of DAO class for recipe requests
 */
public class RecipeRequestDaoImpl extends AbstractDao<RecipeRequest> implements RecipeRequestDao<RecipeRequest> {
    private static final String REQUEST_RECIPE = "insert into recipe_request (recipe_id, date_for) values (?, ?)";
    private final String RECIPE_REQUESTED = "update recipe  set requested = true where recipe_id = ?";
    private static final String GET_RECIPE_REQUESTS_FOR_DOCTOR =
            "select * from recipe_request, recipe, medicine, user where seen = 0 and recipe_request.recipe_id = recipe.recipe_id " +
                    "and recipe.medicine_id = medicine.medicine_id and recipe.user_id = user.user_id";
    private static final String FIND_BY_ID =
            "select * from recipe_request, recipe, medicine, user  where request_id = ? and recipe_request.recipe_id = recipe.recipe_id " +
                    "and recipe.medicine_id = medicine.medicine_id and recipe.user_id = user.user_id";
    private static final String ACCEPT_REQUEST = "update recipe_request  set seen = true where request_id = ?";
    private static final String DENY_REQUEST = "update recipe_request  set seen = true, denied = true where request_id = ?";

    private ProxyConnection proxyConnection;

    public RecipeRequestDaoImpl(ProxyConnection proxyConnection) {
        super(proxyConnection);
        this.proxyConnection = proxyConnection;
    }

    /**
     * @see RecipeRequestDao#requestRecipe(int, String)
     */
    @Override
    public void requestRecipe(int recipeId, String date) throws DaoException {
        Savepoint savepoint;
        try {
            try {
                proxyConnection.setAutoCommit(false);
                savepoint = proxyConnection.setSavepoint();
                try {
                    executeUpdate(REQUEST_RECIPE, Integer.toString(recipeId), date);
                    executeUpdate(RECIPE_REQUESTED, Integer.toString(recipeId));
                    proxyConnection.commit();
                } catch (SQLException e) {
                    proxyConnection.rollback(savepoint);
                    throw new DaoException("error requesting recipe", e);
                }
            } finally {
                proxyConnection.setAutoCommit(true);
            }
        } catch (SQLException e) {
            throw new DaoException("error requesting recipe", e);
        }
    }

    @Override
    protected String getTableName() {
        return "recipe_request";
    }

    @Override
    public Optional<RecipeRequest> getById(int id) throws DaoException {
        return executeForSingleResult(FIND_BY_ID, new RecipeRequestBuilder(), Integer.toString(id));
    }

    /**
     * @see RecipeRequestDao#getNewRequests()
     */
    @Override
    public List<RecipeRequest> getNewRequests() throws DaoException {
        return executeQuery(GET_RECIPE_REQUESTS_FOR_DOCTOR, new RecipeRequestBuilder());
    }

    /**
     * @see RecipeRequestDao#acceptRequest(int)
     */
    @Override
    public void acceptRequest(int requestId) throws DaoException {
        executeUpdate(ACCEPT_REQUEST, Integer.toString(requestId));
    }

    /**
     * @see RecipeRequestDao#denyRequest(int)
     */
    @Override
    public void denyRequest(int requestId) throws DaoException {
        executeUpdate(DENY_REQUEST, Integer.toString(requestId));
    }
}
