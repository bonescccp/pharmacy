package by.epam.pharmacy.web.command.doctor;

import by.epam.pharmacy.service.RecipeService;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.service.validator.Validator;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command for giving recipe to user
 */
public class GiveRecipeCommand implements Command {
    private ServiceFactory serviceFactory;
    private Validator validator;

    public GiveRecipeCommand(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
        validator = new Validator();
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String userId = request.getParameter("user_id");
        String medicineId = request.getParameter("medicine_id");
        String stringDate = request.getParameter("date");
        if (validator.validateDate(stringDate)) {
            RecipeService recipeService = serviceFactory.createRecipeService();
            recipeService.giveRecipe(userId, medicineId, stringDate);
            return new CommandResult("controller?command=show_all_recipes", false);
        } else {
            throw new IllegalArgumentException("Wrong date for recipe. Date :" + stringDate);
        }
    }
}

