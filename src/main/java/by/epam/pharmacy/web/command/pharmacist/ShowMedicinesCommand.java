package by.epam.pharmacy.web.command.pharmacist;

import by.epam.pharmacy.entity.Medicine;
import by.epam.pharmacy.service.MedicineService;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;
import by.epam.pharmacy.web.command.Paginator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Command for showing all medicines
 */
public class ShowMedicinesCommand implements Command {
    private ServiceFactory serviceFactory;
    private Paginator<Medicine> paginator;

    public ShowMedicinesCommand(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
        paginator = new Paginator<>();
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        MedicineService medicineService = serviceFactory.createMedicineService();
        List<Medicine> medicines = medicineService.getMedicines();
        int page = 1;
        int recordsPerPage = 5;
        if (request.getParameter("page") != null)
            page = Integer.parseInt(request.getParameter("page"));
        int numberOfRecords = medicines.size();
        List<Medicine> pageList = paginator.paginate(medicines, page, recordsPerPage);
        int numberOfPages = paginator.countPages(numberOfRecords, recordsPerPage);
        request.setAttribute("numberOfPages", numberOfPages);
        request.setAttribute("currentPage", page);
        request.setAttribute("medicines", pageList);
        return new CommandResult("/web-page/pharmacist.jsp", true);
    }
}
