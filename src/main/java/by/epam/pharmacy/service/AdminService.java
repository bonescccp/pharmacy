package by.epam.pharmacy.service;
/**
 * Service for admin
 */

import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.DaoFactory;
import by.epam.pharmacy.dao.UserDao;
import by.epam.pharmacy.entity.User;

import java.util.List;
import java.util.Optional;

public class AdminService implements Service {
    private DaoFactory daoFactory;

    public AdminService(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    /**
     * if user active blocks him and unblocks when hi is blocked
     *
     * @param id users id(user who we want to block/unblock)
     * @throws ServiceException when error with dao
     */
    public void deleteUserById(int id) throws ServiceException {
        UserDao userDao = daoFactory.createUserDaoImpl();
        try {
            Optional<User> user = userDao.getById(id);
            if (user.isPresent()) {
                if (user.get().isBlocked()) {
                    userDao.unlockById(id);
                } else {
                    userDao.blockById(id);
                }
            }
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public List<User> getUsers() throws ServiceException {
        UserDao userDao = daoFactory.createUserDaoImpl();
        try {
            return userDao.getAll();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}

