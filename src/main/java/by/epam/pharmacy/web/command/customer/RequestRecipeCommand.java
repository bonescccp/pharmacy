package by.epam.pharmacy.web.command.customer;

import by.epam.pharmacy.service.RecipeService;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.service.validator.Validator;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command for requesting recipe extension with data validation
 */
public class RequestRecipeCommand implements Command {
    private ServiceFactory serviceFactory;
    private Validator validator;

    public RequestRecipeCommand(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
        validator = new Validator();
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String recipeId = request.getParameter("request");
        String stringDate = request.getParameter("date");
        if (validator.validateDate(stringDate)) {
            int id = Integer.parseInt(recipeId);
            RecipeService recipeService = serviceFactory.createRecipeService();
            recipeService.requestRecipe(id, stringDate);
            return new CommandResult("controller?command=show_recipes", false);
        } else {
            throw new IllegalArgumentException("Wrong date in request. Date :" + stringDate);
        }
    }
}

