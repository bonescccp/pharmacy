package by.epam.pharmacy.dao.impl;

import by.epam.pharmacy.dao.AbstractDao;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.RecipeDao;
import by.epam.pharmacy.dao.connection.ProxyConnection;
import by.epam.pharmacy.entity.Recipe;
import by.epam.pharmacy.entity.builder.RecipeBuilder;

import java.util.Date;
import java.util.List;

/**
 * implementation of DAO class for recipes
 */
public class RecipeDaoImpl extends AbstractDao<Recipe> implements RecipeDao<Recipe> {
    private static final String GET_RECIPES =
            "select * from recipe, medicine, user where recipe.user_id = ? and medicine.medicine_id = recipe.medicine_id and user.user_id = recipe.user_id";
    private static final String GET_ALL_RECIPES =
            "select * from recipe, medicine, user where medicine.medicine_id = recipe.medicine_id and user.user_id = recipe.user_id";
    private static final String GIVE_RECIPE = "insert into recipe (user_id, medicine_id, expire_date) " +
            "values (?, ?, ?)";
    private static final String EXTEND_RECIPE =
            "update recipe  set expire_date = ? where recipe_id = ?";

    public RecipeDaoImpl(ProxyConnection proxyConnection) {
        super(proxyConnection);
    }

    @Override
    protected String getTableName() {
        return "recipe";
    }

    /**
     * @see RecipeDao#getUserRecipes(int)
     */
    @Override
    public List<Recipe> getUserRecipes(int id) throws DaoException {
        return executeQuery(GET_RECIPES, new RecipeBuilder(), Integer.toString(id));
    }

    @Override
    public List<Recipe> getAll() throws DaoException {
        return executeQuery(GET_ALL_RECIPES, new RecipeBuilder());
    }

    /**
     * @see RecipeDao#giveRecipe(String, String, String)
     */
    @Override
    public void giveRecipe(String userId, String medicineId, String date) throws DaoException {
        executeUpdate(GIVE_RECIPE, userId, medicineId, date);
    }

    /**
     * @see RecipeDao#extendRecipe(int, Date)
     */
    @Override
    public void extendRecipe(int requestId, Date date) throws DaoException {
        executeUpdate(EXTEND_RECIPE, date.toString(), Integer.toString(requestId));
    }
}
