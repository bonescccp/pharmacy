package by.epam.pharmacy.dao;

import by.epam.pharmacy.entity.Identifiable;
import by.epam.pharmacy.entity.User;

import java.util.List;
import java.util.Optional;

/**
 * DAO interface for users
 */
public interface UserDao<T extends Identifiable> extends Dao<T> {
    Optional<User> findUserByLoginAndPassword(String login, String password)
            throws DaoException;

    /**
     * makes some user inactive (by id)
     *
     * @param id users id
     * @throws DaoException when something wrong with executing query
     */
    void blockById(int id) throws DaoException;

    /**
     * makes some user active (by id)
     *
     * @param id users id
     * @throws DaoException when something wrong with executing query
     */
    void unlockById(int id) throws DaoException;

    /**
     * add to users balance some money
     *
     * @param id     user id
     * @param amount amount of money
     * @throws DaoException when something wrong with executing query
     */
    void addMoney(int id, String amount) throws DaoException;

    /**
     * @return list of users with role user
     * @throws DaoException when something wrong with executing query
     */
    List<User> getCustomers() throws DaoException;
}

