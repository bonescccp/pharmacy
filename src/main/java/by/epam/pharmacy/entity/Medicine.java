package by.epam.pharmacy.entity;

import by.epam.pharmacy.entity.entity_help.MedicineForm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Medicine class
 */
public class Medicine implements Identifiable, Serializable {

    private static final long serialVersionUID = 1L;

    private final int id;
    private final String title;
    private final BigDecimal price;
    private final MedicineForm form;
    private final boolean recipe;
    private final boolean available;

    public Medicine(int id, String title, BigDecimal price,
                    MedicineForm form, boolean recipe, boolean available) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.form = form;
        this.recipe = recipe;
        this.available = available;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public MedicineForm getForm() {
        return form;
    }

    public boolean isRecipe() {
        return recipe;
    }

    public boolean isAvailable() {
        return available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Medicine medicine = (Medicine) o;
        return id == medicine.id &&
                recipe == medicine.recipe &&
                available == medicine.available &&
                Objects.equals(title, medicine.title) &&
                Objects.equals(price, medicine.price) &&
                form == medicine.form;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, price, form, recipe, available);
    }
}
