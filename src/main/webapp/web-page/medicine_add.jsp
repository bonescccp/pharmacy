<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="/web-page/header.jsp" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="pharmacist"/>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/pages.css">
    <title><fmt:message key="label.headeradd"/></title>
</head>
<body>
<div class="content">
    <form class="form" method="post" action="${pageContext.request.contextPath}/controller?command=add_medicine">
        <div class="form-inner">
            <h2><fmt:message key="label.add"/></h2>
            <div class="form-content">
                <h3><fmt:message key="label.title"/></h3>
                <p>
                    <input type="text" name="title" id="title" placeholder=
                    <fmt:message key="label.title"/> required pattern="[A-Za-z]{3,8}[0-9A-Fa-f]{0,8}">
                </p>
                <h3><fmt:message key="label.price"/></h3>
                <p>
                    <input type="number" name="price" id="price" placeholder=
                    <fmt:message key="label.price"/> required min="0.01" step="0.01" max="999.99"/>
                </p>
                <h3><fmt:message key="label.form"/></h3>
                <p>
                    <select size="0" name="form" required>
                        <option disabled><fmt:message key="label.selectform"/></option>
                        <option value="pill"><fmt:message key="label.pill"/></option>
                        <option value="capsule"><fmt:message key="label.capsule"/></option>
                        <option value="injection"><fmt:message key="label.injection"/></option>
                        <option value="syrup"><fmt:message key="label.syrup"/></option>
                    </select>
                </p>
                <h3><fmt:message key="label.recipe"/></h3>
                <p>
                    <select size="0" name="recipe" required>
                        <option disabled><fmt:message key="label.recipeselect"/></option>
                        <option value="0"><fmt:message key="label.free"/></option>
                        <option value="1"><fmt:message key="label.byrecipe"/></option>
                    </select>
                </p>
                <h3><fmt:message key="label.available"/></h3>
                <p>
                    <select size="0" name="available" required>
                        <option disabled><fmt:message key="label.availableselect"/></option>
                        <option value="0"><fmt:message key="label.navailable"/></option>
                        <option value="1"><fmt:message key="label.available"/></option>
                    </select>
                </p>
                <p><input type="submit" value=<fmt:message key="label.add"/>></p>

            </div>
        </div>
    </form>
</div>
<div class="category-wrap">
    <h3><fmt:message key="label.menu"/></h3>
    <ul>
        <li><a href="${pageContext.request.contextPath}/controller?command=show_medicines"><fmt:message
                key="label.home"/></a>
        <li><a href="${pageContext.request.contextPath}/web-page/medicine_add.jsp"><fmt:message key="label.add"/></a>
        <li><a href="${pageContext.request.contextPath}/controller?command=logout"><fmt:message key="label.logout"/></a>
    </ul>
</div>
</body>
</html>