package by.epam.pharmacy.dao.impl;

import by.epam.pharmacy.dao.AbstractDao;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.OrderDao;
import by.epam.pharmacy.dao.connection.ProxyConnection;
import by.epam.pharmacy.entity.Order;
import by.epam.pharmacy.entity.builder.BuilderFactory;
import by.epam.pharmacy.entity.builder.OrderBuilder;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.List;
import java.util.Optional;

/**
 * implementation of DAO class for orders
 */
public class OrderDaoImpl extends AbstractDao<Order> implements OrderDao<Order> {
    private static final String GET_USERS_ORDERS =
            "select * from user_order, medicine where user_order.payed = false and  user_id = ? and user_order.medicine_id = medicine.medicine_id";
    private static final String GET_ARCHIVE_USERS_ORDERS =
            "select * from user_order, medicine where user_order.payed = true and  user_id = ? and user_order.medicine_id = medicine.medicine_id";
    private static final String ORDER = "insert into user_order (user_id, medicine_id, quantity) values" +
            " (?, ?, ?)";
    private static final String DELETE_ORDER = "delete from user_order where order_id= ?";
    private static final String CHANGE_ORDER =
            "update user_order  set quantity = ? where order_id = ?";
    private static final String PAY_ORDER =
            "update user_order  set payed = true where order_id = ?";
    private static final String WITHDRAW_MONEY =
            "update user  set money = money - ? where user_id = ?";
    private static final String FIND_BY_ID =
            "select * from user_order, medicine where order_id = ? and user_order.medicine_id = medicine.medicine_id";
    private ProxyConnection proxyConnection;

    public OrderDaoImpl(ProxyConnection proxyConnection) {
        super(proxyConnection);
        this.proxyConnection = proxyConnection;
    }

    @Override
    protected String getTableName() {
        return "user_order";
    }

    /**
     * @see OrderDao#order(int, String, String)
     */
    @Override
    public void order(int userId, String medId, String quantity) throws DaoException {
        executeUpdate(ORDER, Integer.toString(userId), medId, quantity);
    }

    /**
     * @see OrderDao#getUsersOrders(int)
     */
    @Override
    public List<Order> getUsersOrders(int userId) throws DaoException {
        return executeQuery(GET_USERS_ORDERS, new OrderBuilder(), Integer.toString(userId));
    }

    /**
     * @see OrderDao#getArchiveUsersOrders(int)
     */
    @Override
    public List<Order> getArchiveUsersOrders(int userId) throws DaoException {
        return executeQuery(GET_ARCHIVE_USERS_ORDERS, new OrderBuilder(), Integer.toString(userId));
    }

    /**
     * @see OrderDao#deleteOrder(String)
     */
    @Override
    public void deleteOrder(String orderId) throws DaoException {
        executeUpdate(DELETE_ORDER, orderId);
    }

    /**
     * @see OrderDao#changeOrder(String, String)
     */
    @Override
    public void changeOrder(String orderId, String quantity) throws DaoException {
        executeUpdate(CHANGE_ORDER, quantity, orderId);
    }

    /**
     * @see OrderDao#payOrder(String, BigDecimal, int)
     */
    @Override
    public void payOrder(String orderId, BigDecimal cost, int userID) throws DaoException {
        Savepoint savepoint;
        try {
            try {
                proxyConnection.setAutoCommit(false);
                savepoint = proxyConnection.setSavepoint();
                try {
                    executeUpdate(PAY_ORDER, orderId);
                    executeUpdate(WITHDRAW_MONEY, cost.toString(), Integer.toString(userID));
                    proxyConnection.commit();
                } catch (SQLException e) {
                    proxyConnection.rollback(savepoint);
                    throw new DaoException("error paying order", e);
                }
            } finally {
                proxyConnection.setAutoCommit(true);
            }
        } catch (SQLException e) {
            throw new DaoException("error paying order", e);
        }
    }

    /**
     * @see AbstractDao#getById(int)
     */
    @Override
    public Optional<Order> getById(int id) throws DaoException {
        return executeForSingleResult(
                FIND_BY_ID,
                BuilderFactory.getBuilder("user_order"),
                Integer.toString(id)
        );
    }
}
