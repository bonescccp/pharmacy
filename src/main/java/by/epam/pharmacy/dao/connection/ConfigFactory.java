package by.epam.pharmacy.dao.connection;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Class for creating configuration for data base connection from file
 */
public class ConfigFactory {
    private String filePath;


    /**
     * @param filePath path to the properties file
     */
    public ConfigFactory(String filePath) {
        this.filePath = filePath;
    }

    /**
     * @return object contains url, user, password and size of pool from properties file
     * @throws IOException when something wrong with file
     */
    public DataBaseConfig getDatabaseConfig() throws IOException {

        InputStream inputStream = new FileInputStream(filePath);
        Properties prop = new Properties();
        prop.load(inputStream);
        String url = prop.getProperty("url");
        String user = prop.getProperty("user");
        String password = prop.getProperty("password");
        int poolSize = Integer.parseInt(prop.getProperty("poolSize"));
        DataBaseConfig config = new DataBaseConfig(url, user, password, poolSize);
        return config;
    }
}
