package by.epam.pharmacy.dao.connection;


public class ConnectionToDataBaseException extends RuntimeException {

    public ConnectionToDataBaseException() {
    }

    public ConnectionToDataBaseException(String message) {
        super(message);
    }


    public ConnectionToDataBaseException(String message, Throwable cause) {
        super(message, cause);
    }


    public ConnectionToDataBaseException(Throwable cause) {
        super(cause);
    }
}
