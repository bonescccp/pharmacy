package by.epam.pharmacy.web.filter;

import by.epam.pharmacy.entity.User;
import by.epam.pharmacy.entity.entity_help.UserRole;
import by.epam.pharmacy.web.filter.list.AllowedCommandsList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Security filter
 */
public class AccessFilter implements Filter {
    private FilterConfig filterConfig;
    private static Logger logger = LogManager.getLogger(AccessFilter.class);

    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        UserRole userRole;
        HttpSession session = req.getSession(false);
        String command = req.getParameter("command");
        List<String> allowedCommands;
        User user = null;
        if (session != null) {
            user = (User) session.getAttribute("user");
        }
        if (user == null) {
            allowedCommands = AllowedCommandsList.getNullUserCommands();
            if (command == null || allowedCommands.contains(command)) {
                chain.doFilter(request, response);
                return;
            }
        } else {
            userRole = user.getRole();
            allowedCommands = AllowedCommandsList.getAllowedCommands(userRole);
            if (allowedCommands.contains(command) || command == null) {
                chain.doFilter(request, response);
                return;
            }
        }
        logger.error(new IllegalAccessException("Attempt to illegal access!"));
        resp.sendRedirect("pharmacy/error.jsp");
    }

    public void destroy() {
    }
}

