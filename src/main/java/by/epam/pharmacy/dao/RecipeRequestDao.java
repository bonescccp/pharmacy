package by.epam.pharmacy.dao;

import by.epam.pharmacy.entity.Identifiable;
import by.epam.pharmacy.entity.RecipeRequest;

import java.util.List;

/**
 * DAO interface for recipe requests
 */
public interface RecipeRequestDao<T extends Identifiable> extends Dao<T> {
    /**
     * creates request for recipe extension with date
     *
     * @param recipeId id of recipe
     * @param date     date for extension
     * @throws DaoException when something wrong with executing query
     */
    void requestRecipe(int recipeId, String date) throws DaoException;

    /**
     * extends recipe and makes request seen
     *
     * @param requestId id of request
     * @throws DaoException when something wrong with executing query
     */
    void acceptRequest(int requestId) throws DaoException;

    /**
     * returns active requests
     *
     * @return list jf neq requests (not seen)
     * @throws DaoException when something wrong with executing query
     */
    List<RecipeRequest> getNewRequests() throws DaoException;

    /**
     * makes request seen but not extends recipe
     *
     * @param requestId id of request
     * @throws DaoException when something wrong with executing query
     */
    void denyRequest(int requestId) throws DaoException;
}
