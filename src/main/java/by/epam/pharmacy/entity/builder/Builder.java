package by.epam.pharmacy.entity.builder;

import by.epam.pharmacy.entity.Identifiable;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Entity builder interface with method build
 */
public interface Builder<T extends Identifiable> {

    T build(ResultSet resultSet) throws SQLException;
}