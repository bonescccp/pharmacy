package by.epam.pharmacy.web.command;

import by.epam.pharmacy.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Class for getting user id from session
 */
public class UserIdGetter {
    public static int getUserId(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return 0;
        }
        User user = (User) session.getAttribute("user");
        if (user == null) {
            return 0;
        }
        return user.getId();
    }
}
