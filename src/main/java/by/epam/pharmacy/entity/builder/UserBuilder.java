package by.epam.pharmacy.entity.builder;

import by.epam.pharmacy.entity.User;
import by.epam.pharmacy.entity.entity_help.UserRole;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Builder class for recipe
 */
public class UserBuilder implements Builder<User> {
    private static final String ID = "user_id";
    private static final String LOGIN = "user_login";
    private static final String PASSWORD = "user_password";
    private static final String ROLE = "user_role";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String MONEY = "money";
    private static final String BLOCKED = "blocked";

    @Override
    public User build(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt(ID);
        String login = resultSet.getString(LOGIN);
        String password = resultSet.getString(PASSWORD);
        String role = resultSet.getString(ROLE);
        String firstName = resultSet.getString(FIRST_NAME);
        String lastName = resultSet.getString(LAST_NAME);
        BigDecimal money = resultSet.getBigDecimal(MONEY);
        int blocked = resultSet.getInt(BLOCKED);

        UserRole userRole = UserRole.valueOf(role.toUpperCase());
        boolean userStatus = blocked == 1;
        return new User(id, login, password, userRole, firstName, lastName, money, userStatus);
    }
}
