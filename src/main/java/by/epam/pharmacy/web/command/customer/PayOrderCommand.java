package by.epam.pharmacy.web.command.customer;


import by.epam.pharmacy.entity.Medicine;
import by.epam.pharmacy.entity.Order;
import by.epam.pharmacy.entity.User;
import by.epam.pharmacy.service.CustomerService;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;
import by.epam.pharmacy.web.command.CommandUtil;
import by.epam.pharmacy.web.command.UserIdGetter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Command for completing order with validation of medicine availability
 */
public class PayOrderCommand implements Command {
    private ServiceFactory serviceFactory;

    public PayOrderCommand(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        int userId = UserIdGetter.getUserId(request);
        CommandUtil commandUtil = new CommandUtil(serviceFactory);
        List<Medicine> availableMedicines = commandUtil.getAvailableMedicine(userId);
        String orderId = request.getParameter("order_id");
        CustomerService customerService = serviceFactory.createCustomerService();
        Order order = customerService.getById(Integer.parseInt(orderId)).get();
        Medicine medicine = order.getMedicine();
        if (availableMedicines.contains(medicine)) {
            customerService.payOrder(orderId, userId);
            User user = customerService.getUserById(userId).get();
            HttpSession session = request.getSession(false);
            session.setAttribute("user", user);
            return new CommandResult("controller?command=show_archive_orders", false);
        }
        throw new IllegalArgumentException("Attempt to buy " + medicine);
    }
}
