package by.epam.pharmacy.service;

import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.DaoFactory;
import by.epam.pharmacy.dao.RecipeDao;
import by.epam.pharmacy.dao.RecipeRequestDao;
import by.epam.pharmacy.entity.Recipe;
import by.epam.pharmacy.entity.RecipeRequest;

import java.util.Date;
import java.util.List;

/**
 * Service for managing recipes
 */
public class RecipeService implements Service {
    private DaoFactory daoFactory;

    public RecipeService(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    public List<Recipe> getRecipes(int id) throws ServiceException {
        RecipeDao recipeDao = daoFactory.createRecipeDaoImpl();
        try {
            return recipeDao.getUserRecipes(id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public List<Recipe> getAll() throws ServiceException {
        RecipeDao recipeDao = daoFactory.createRecipeDaoImpl();
        try {
            return recipeDao.getAll();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public List<RecipeRequest> getRecipeRequests() throws ServiceException {
        RecipeRequestDao recipeRequestDao = daoFactory.createRecipeRequestDaoImpl();
        try {
            return recipeRequestDao.getNewRequests();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public void requestRecipe(int recipeId, String date) throws ServiceException {
        RecipeRequestDao recipeRequestDao = daoFactory.createRecipeRequestDaoImpl();
        try {
            recipeRequestDao.requestRecipe(recipeId, date);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public void giveRecipe(String userId, String medicineId, String date) throws ServiceException {
        RecipeDao recipeDao = daoFactory.createRecipeDaoImpl();
        try {
            recipeDao.giveRecipe(userId, medicineId, date);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public void acceptRequest(int requestId) throws ServiceException {
        RecipeDao recipeDao = daoFactory.createRecipeDaoImpl();
        RecipeRequestDao recipeRequestDao = daoFactory.createRecipeRequestDaoImpl();
        try {
            RecipeRequest recipeRequest = (RecipeRequest) recipeRequestDao.getById(requestId).get();
            Date date = recipeRequest.getDate();
            int recipeId = recipeRequest.getRecipe().getId();
            recipeDao.extendRecipe(recipeId, date);
            recipeRequestDao.acceptRequest(requestId);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public void denyRequest(int requestId) throws ServiceException {
        RecipeRequestDao recipeRequestDao = daoFactory.createRecipeRequestDaoImpl();
        try {
            recipeRequestDao.denyRequest(requestId);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}





