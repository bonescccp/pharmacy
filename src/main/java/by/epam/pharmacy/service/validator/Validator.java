package by.epam.pharmacy.service.validator;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class for data validation
 */
public class Validator {
    public static final int MINIMUM_QUANTITY = 0;
    public static final int MAXIMUM_QUANTITY = 100;
    public static final double MINIMUM_MONEY = 0.01;
    public static final double MAXIMUM_MONEY = 1000.00;

    public boolean validateQuantity(String stringQuantity) {
        int quantity = Integer.parseInt(stringQuantity);
        return quantity > MINIMUM_QUANTITY && quantity < MAXIMUM_QUANTITY;
    }

    public boolean validateMoney(String quantity) {
        BigDecimal money = new BigDecimal(quantity);
        return money.compareTo(new BigDecimal(MINIMUM_MONEY)) > 0 && money.compareTo(new BigDecimal(MAXIMUM_MONEY)) < 0;
    }

    public boolean validateDate(String stringDate) {
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
        Date now = new Date();
        Date requested;
        try {
            requested = sd.parse(stringDate);
        } catch (ParseException e) {
            return false;
        }
        return !now.after(requested);
    }

    public boolean validateTitle(String regex, String title) {
        return title.matches(regex);
    }
}
