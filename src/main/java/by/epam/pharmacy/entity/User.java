package by.epam.pharmacy.entity;

import by.epam.pharmacy.entity.entity_help.UserRole;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * User class
 */
public class User implements Identifiable, Serializable {

    private static final long serialVersionUID = 1L;

    private final int id;
    private final String login;
    private final String password;
    private final UserRole role;
    private final String firstName;
    private final String lastName;
    private final BigDecimal money;
    private final boolean blocked;

    public User(int id, String login, String password, UserRole role, String firstName,
                String lastName, BigDecimal money, boolean blocked) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.role = role;
        this.firstName = firstName;
        this.lastName = lastName;
        this.money = money;
        this.blocked = blocked;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public UserRole getRole() {
        return role;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public boolean isBlocked() {
        return blocked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return id == user.id &&
                blocked == user.blocked &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                role == user.role &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(money, user.money);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, password, role, firstName, lastName, money, blocked);
    }
}
