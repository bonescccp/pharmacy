package by.epam.pharmacy.web.command.customer;

import by.epam.pharmacy.entity.Medicine;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.web.command.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ShowAvailableMedicines implements Command {
    private ServiceFactory serviceFactory;
    private Paginator<Medicine> paginator;

    /**
     * Command for showing available for user medicines
     */
    public ShowAvailableMedicines(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
        paginator = new Paginator<>();
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        CommandUtil commandUtil = new CommandUtil(serviceFactory);
        int userId = UserIdGetter.getUserId(request);
        List<Medicine> availableMedicines = commandUtil.getAvailableMedicine(userId);
        int page = 1;
        int recordsPerPage = 5;
        if (request.getParameter("page") != null)
            page = Integer.parseInt(request.getParameter("page"));
        int numberOfRecords = availableMedicines.size();
        List<Medicine> pageList = paginator.paginate(availableMedicines, page, recordsPerPage);
        int numberOfPages = paginator.countPages(numberOfRecords, recordsPerPage);
        request.setAttribute("numberOfPages", numberOfPages);
        request.setAttribute("currentPage", page);
        request.setAttribute("users", pageList);
        request.setAttribute("available_medicines", pageList);
        return new CommandResult("/web-page/user.jsp", true);
    }
}
