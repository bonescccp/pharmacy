package by.epam.pharmacy.web.command.user;

import by.epam.pharmacy.entity.User;
import by.epam.pharmacy.entity.entity_help.UserRole;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.service.UserService;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Optional;

/**
 * Command for signing in
 */
public class LoginCommand implements Command {
    private ServiceFactory serviceFactory;

    public LoginCommand(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        CommandResult commandResult;
        UserService userService = serviceFactory.createUserService();
        Optional<User> user = userService.findUserByLoginAndPassword(login, password);
        if (user.isPresent()) {
            if (user.get().isBlocked()) {
                commandResult = new CommandResult("login.jsp", true);
                request.getSession().invalidate();
                request.setAttribute("error", "User deleted!");
                return commandResult;
            }
            HttpSession session = request.getSession();
            session.setAttribute("user", user.get());
            UserRole userRole = user.get().getRole();
            commandResult = endSignIn(userRole);
            return commandResult;
        } else {
            commandResult = new CommandResult("login.jsp", true);
            request.setAttribute("error", "Incorrect username or password");
            return commandResult;
        }
    }

    private CommandResult endSignIn(UserRole userRole) {
        switch (userRole) {
            case ADMIN:
                return new CommandResult("controller?command=show_users", false);
            case DOCTOR:
                return new CommandResult("controller?command=show_requests", false);
            case PHARMACIST:
                return new CommandResult("controller?command=show_medicines", false);
            case USER:
                return new CommandResult("controller?command=available_medicine", false);
            default:
                throw new RuntimeException("Unknown user role!");
        }
    }
}


