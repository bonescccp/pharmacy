package by.epam.pharmacy.entity;

/**
 * Simple class for transfer data in String to write medicine in table
 */
public class MedicineParams {
    private final String title;
    private final String price;
    private final String form;
    private final String recipe;
    private final String available;

    public MedicineParams(String title, String price,
                          String form, String recipe, String available) {
        this.title = title;
        this.price = price;
        this.form = form;
        this.recipe = recipe;
        this.available = available;
    }

    public String getTitle() {
        return title;
    }

    public String getPrice() {
        return price;
    }

    public String getForm() {
        return form;
    }

    public String getRecipe() {
        return recipe;
    }

    public String getAvailable() {
        return available;
    }
}
