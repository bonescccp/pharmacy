package by.epam.pharmacy.web.command;

/**
 * class for post redirect get implementation
 */
public class CommandResult {
    private final boolean forward;
    private final String page;

    public CommandResult(String page, boolean forward) {
        this.page = page;
        this.forward = forward;
    }

    public boolean isForward() {
        return forward;
    }

    public String getPage() {
        return page;
    }

}
