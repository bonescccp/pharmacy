package by.epam.pharmacy.entity.builder;

import by.epam.pharmacy.entity.Medicine;
import by.epam.pharmacy.entity.Recipe;
import by.epam.pharmacy.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Builder class for recipe
 */
public class RecipeBuilder implements Builder<Recipe> {
    private static final String ID = "recipe_id";
    private static final String DATE = "expire_date";
    private static final String REQUESTED = "requested";

    private Builder medicineBuilder;
    private Builder userBuilder;

    public RecipeBuilder() {
        medicineBuilder = new MedicineBuilder();
        userBuilder = new UserBuilder();
    }


    @Override
    public Recipe build(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt(ID);
        Date expDate = resultSet.getDate(DATE);
        int requested = resultSet.getInt(REQUESTED);

        boolean isRequested = requested == 1;
        Date now = new Date();
        boolean valid = expDate.after(now);
        User user = (User) userBuilder.build(resultSet);
        Medicine medicine = (Medicine) medicineBuilder.build(resultSet);
        return new Recipe(id, expDate, isRequested, medicine, user, valid);
    }
}
