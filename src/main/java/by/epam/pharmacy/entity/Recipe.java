package by.epam.pharmacy.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * recipe class
 */
public class Recipe implements Identifiable, Serializable {

    private static final long serialVersionUID = 1L;

    private final int id;
    private final Date expDate;
    private final boolean requested;
    private final boolean valid;
    private final Medicine medicine;
    private final User user;


    public Recipe(int id, Date expDate, boolean requested, Medicine medicine, User user, boolean valid) {
        this.id = id;
        this.expDate = expDate;
        this.requested = requested;
        this.valid = valid;
        this.medicine = medicine;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public Date getExpDate() {
        return expDate;
    }

    public boolean isRequested() {
        return requested;
    }

    public boolean isValid() {
        return valid;
    }

    public Medicine getMedicine() {
        return medicine;
    }

    public User getUser() {
        return user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Recipe recipe = (Recipe) o;
        return id == recipe.id &&
                requested == recipe.requested &&
                valid == recipe.valid &&
                Objects.equals(expDate, recipe.expDate) &&
                Objects.equals(medicine, recipe.medicine) &&
                Objects.equals(user, recipe.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, expDate, requested, valid, medicine, user);
    }
}
