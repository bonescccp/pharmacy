package by.epam.pharmacy.entity.entity_help;

/**
 * Forms of medicine enum
 */
public enum MedicineForm {
    PILL("pill"),
    CAPSULE("capsule"),
    INJECTION("injection"),
    SYRUP("syrup");

    private String title;

    MedicineForm(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}