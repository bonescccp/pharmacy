package by.epam.pharmacy.entity.entity_help;

/**
 * User roles enum
 */
public enum UserRole {
    ADMIN("admin"),
    DOCTOR("doctor"),
    PHARMACIST("pharmacist"),
    USER("user");

    private String title;

    UserRole(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
