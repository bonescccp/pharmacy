# EPAM pharmacy

This is final task web project of external java training in EPAM. It was created in 2019 in Minsk, Belarus.
It is a web pharmacy. 

## Getting Started

Project developed to work with Apache TomCat. 

## Deployment

To deploy project install it by maven and deploy on apache TomCat server.

## Built With

Apache Maven
IntelliJ IDEA

## Authors

Tsimur Tarashkevich

## License

This project is licensed under the  free license.

