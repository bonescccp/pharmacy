package by.epam.pharmacy.dao.impl;

import by.epam.pharmacy.dao.AbstractDao;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.UserDao;
import by.epam.pharmacy.dao.connection.ProxyConnection;
import by.epam.pharmacy.entity.User;
import by.epam.pharmacy.entity.builder.BuilderFactory;
import by.epam.pharmacy.entity.entity_help.UserRole;

import java.util.List;
import java.util.Optional;

/**
 * implementation of DAO class for user
 */
public class UserDaoImpl extends AbstractDao<User> implements UserDao<User> {
    private static final String FIND_BY_LOGIN_AND_PASSWORD =
            "select * from user where user_login = ? and user_password = ?";
    private static final String FIND_BY_ID =
            "select * from user where user_id = ?";
    private static final String DELETE_BY_ID =
            "update user  set blocked = ? where user_id = ?";
    private static final String ADD_MONEY =
            "update user  set money = money + ? where user_id = ?";
    private static final String GET_CUSTOMERS =
            "select * from user where user_role = ? and blocked = false";


    public UserDaoImpl(ProxyConnection proxyConnection) {
        super(proxyConnection);
    }

    @Override
    public Optional<User> findUserByLoginAndPassword(String login, String password) throws DaoException {
        return executeForSingleResult(
                FIND_BY_LOGIN_AND_PASSWORD,
                BuilderFactory.getBuilder("user"),
                login,
                password
        );
    }

    @Override
    public Optional<User> getById(int id) throws DaoException {
        return executeForSingleResult(
                FIND_BY_ID,
                BuilderFactory.getBuilder("user"),
                Integer.toString(id)
        );
    }

    /**
     * @see UserDao#blockById(int)
     */
    @Override
    public void blockById(int id) throws DaoException {
        executeUpdate(DELETE_BY_ID, "1", Integer.toString(id));
    }

    /**
     * @see UserDao#unlockById(int)
     */
    @Override
    public void unlockById(int id) throws DaoException {
        executeUpdate(DELETE_BY_ID, "0", Integer.toString(id));
    }

    /**
     * @see UserDao#addMoney(int, String)
     */
    @Override
    public void addMoney(int id, String amount) throws DaoException {
        executeUpdate(ADD_MONEY, amount, Integer.toString(id));
    }

    @Override
    protected String getTableName() {
        return "user";
    }

    /**
     * @see UserDao#getCustomers()
     */
    @Override
    public List<User> getCustomers() throws DaoException {
        return executeQuery(GET_CUSTOMERS, BuilderFactory.getBuilder("user"), UserRole.USER.toString());
    }
}
