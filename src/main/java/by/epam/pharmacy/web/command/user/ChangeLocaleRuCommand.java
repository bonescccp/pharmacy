package by.epam.pharmacy.web.command.user;

import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Command for changing locale to russian
 */
public class ChangeLocaleRuCommand implements Command {

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession(false);
        session.setAttribute("lang", "ru");
        String referer = request.getHeader("Referer");
        return new CommandResult(referer, false);
    }
}