<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/web-page/header.jsp" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="admin"/>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/pages.css">
    <title><fmt:message key="label.header"/></title>
</head>
<body>
<div class="content">
    <table class="table">
        <tr>
            <th><fmt:message key="label.login"/></th>
            <th><fmt:message key="label.fname"/></th>
            <th><fmt:message key="label.lname"/></th>
            <th><fmt:message key="label.role"/></th>
            <th><fmt:message key="label.blocked"/></th>
            <th><fmt:message key="label.block"/></th>
        </tr>
        <c:forEach var="user" items="${users}">
            <tr>
                <td>${user.login}</td>
                <td>${user.firstName}</td>
                <td>${user.lastName}</td>
                <td>
                    <c:choose>
                        <c:when test="${user.role == 'ADMIN'}">
                            <fmt:message key="label.admin"/>
                        </c:when>
                        <c:when test="${user.role == 'USER'}">
                            <fmt:message key="label.user"/>
                        </c:when>
                        <c:when test="${user.role == 'DOCTOR'}">
                            <fmt:message key="label.doctor"/>
                        </c:when>
                        <c:when test="${user.role == 'PHARMACIST'}">
                            <fmt:message key="label.pharmacist"/>
                        </c:when>
                    </c:choose>
                </td>
                <td><c:if test="${user.blocked == true}">
                    ✓
                </c:if>
                    <c:if test="${user.blocked != true}">&nbsp
                    </c:if>
                </td>
                <td>
                    <c:if test="${sessionScope.user.id == user.id}">
                        <fmt:message key="label.you"/>
                    </c:if>
                    <c:if test="${sessionScope.user.id != user.id}">
                    <c:if test="${user.blocked == true}">
                    <form onSubmit='return confirm("<fmt:message key="label.confirm"/> ${user.login} ?");' method="post"
                          action="${pageContext.request.contextPath}/controller?command=delete_user">
                        <button type="submit" name="delete" value=${user.id}><fmt:message key="label.unban"/></button>
                        </c:if>
                        <c:if test="${user.blocked == false}">
                        <form onSubmit='return confirm("<fmt:message key="label.confirmb"/> ${user.login} ?");' method="post"
                              action="${pageContext.request.contextPath}/controller?command=delete_user">
                            <button type="submit" name="delete" value=${user.id}><fmt:message key="label.ban"/></button>
                            </c:if>
                            </c:if>
                        </form>
                </td>
            </tr>
        </c:forEach>
        <tr>
            <td class="round-bottom" colspan="6">
                <c:if test="${numberOfPages gt 1}">
                <c:if test="${currentPage != 1}">
                    <a href="controller?command=show_users&page=${currentPage - 1}">←</a>
                </c:if>
                <c:forEach begin="1" end="${numberOfPages}" var="i">
                    <c:choose>
                        <c:when test="${currentPage eq i}">
                            ${i}
                        </c:when>
                        <c:otherwise>
                            <a href="controller?command=show_users&page=${i}">${i}</a>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                <c:if test="${currentPage lt numberOfPages}">
                <a href="controller?command=show_users&page=${currentPage + 1}">→</a></td>
            </c:if>
            </c:if>
            </td>
        </tr>
    </table>
</div>
<div class="category-wrap">
    <h3><fmt:message key="label.menu"/></h3>
    <ul>
        <li><a href="${pageContext.request.contextPath}/controller?command=show_users"><fmt:message
                key="label.home"/></a>
        <li><a href="${pageContext.request.contextPath}/controller?command=logout"><fmt:message key="label.logout"/></a>
    </ul>
</div>
</body>
</html>

