package by.epam.pharmacy.entity.builder;

import by.epam.pharmacy.entity.Medicine;
import by.epam.pharmacy.entity.Order;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Builder class for order
 */
public class OrderBuilder implements Builder<Order> {
    private static final String ID = "order_id";
    private static final String USER_ID = "user_id";
    private static final String QTY = "quantity";
    private static final String PAYED = "payed";

    private Builder medicineBuilder;

    public OrderBuilder() {
        medicineBuilder = new MedicineBuilder();
    }

    @Override
    public Order build(ResultSet resultSet) throws SQLException {

        int id = resultSet.getInt(ID);
        int userId = resultSet.getInt(USER_ID);
        int quantity = resultSet.getInt(QTY);
        int payed = resultSet.getInt(PAYED);

        boolean isPayed = payed == 1;
        Medicine medicine = (Medicine) medicineBuilder.build(resultSet);
        return new Order(id, userId, medicine, quantity, isPayed);
    }
}
