package by.epam.pharmacy.dao;

import by.epam.pharmacy.entity.Identifiable;

import java.util.List;
import java.util.Optional;

/**
 * interface for DAO
 */
public interface Dao<T extends Identifiable> {

    Optional<T> getById(int id) throws DaoException;

    List<T> getAll() throws DaoException;

}
