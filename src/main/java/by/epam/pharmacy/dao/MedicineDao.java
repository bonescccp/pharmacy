package by.epam.pharmacy.dao;

import by.epam.pharmacy.entity.Identifiable;
import by.epam.pharmacy.entity.Medicine;
import by.epam.pharmacy.entity.MedicineParams;

import java.util.List;

/**
 * DAO interface for medicines
 */
public interface MedicineDao<T extends Identifiable> extends Dao<T> {
    /**
     * @return list of medicines that can be bought only by recipe
     * @throws DaoException when something wrong with executing query
     */
    List<Medicine> getRecipedMedicines() throws DaoException;

    /**
     * makes medicine not available
     *
     * @param id id of medicine
     * @throws DaoException when something wrong with executing query
     */
    void arcivateById(int id) throws DaoException;

    /**
     * makes medicine  available if it is in archive
     *
     * @param id id of medicine
     * @throws DaoException when something wrong with executing query
     */
    void unZipById(int id) throws DaoException;

    /**
     * saves medicine with params from dto object
     *
     * @param medicineParams entity of dto object with medicine params
     * @throws DaoException when something wrong with executing query
     */
    void saveMedicine(MedicineParams medicineParams) throws DaoException;
}
