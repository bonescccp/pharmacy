package by.epam.pharmacy.web.filter.list;

import by.epam.pharmacy.entity.entity_help.UserRole;

import java.util.Arrays;
import java.util.List;

/**
 * Lists of allowed commands according to users role
 */
public class AllowedCommandsList {
    private static final List<String> nullUserCommands = Arrays.asList("login", "localeEN", "localeRU", "go_home");
    private static final List<String> adminCommands = Arrays.asList("show_users", "delete_user",
            "localeEN", "localeRU", "go_home", "logout");
    private static final List<String> userCommands = Arrays.asList("change_order", "delete_order",
            "fill_balance", "order", "pay_order", "request_recipe", "available_medicine",
            "show_orders", "show_archive_orders", "show_recipes",
            "localeEN", "localeRU", "go_home", "logout");
    private static final List<String> pharmacistCommands = Arrays.asList("add_medicine", "delete_medicine",
            "show_medicines",
            "localeEN", "localeRU", "go_home", "logout");
    private static final List<String> doctorCommands = Arrays.asList("accept_request", "deny_request",
            "give_recipe", "show_all_recipes", "show_requests", "show_customers",
            "localeEN", "localeRU", "go_home", "logout");

    public static List<String> getNullUserCommands() {
        return nullUserCommands;
    }

    public static List<String> getAllowedCommands(UserRole userRole) {
        switch (userRole) {
            case ADMIN:
                return adminCommands;
            case USER:
                return userCommands;
            case PHARMACIST:
                return pharmacistCommands;
            case DOCTOR:
                return doctorCommands;
        }
        return null;
    }
}
