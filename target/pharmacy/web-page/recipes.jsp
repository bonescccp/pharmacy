<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/web-page/header.jsp" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="user"/>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/pages.css">
    <title><fmt:message key="label.header"/></title>
</head>
<body>
<jsp:useBean id="now" class="java.util.Date"/>
<div class="content">
    <table class="table">
        <c:if test="${fn:length(recipes) gt 0}">
            <tr>
                <th><fmt:message key="label.title"/></th>
                <th><fmt:message key="label.validto"/></th>
                <th><fmt:message key="label.valid"/></th>
                <th><fmt:message key="label.request"/></th>
            </tr>

            <c:forEach var="recipe" items="${recipes}">
                <tr>
                    <td>${recipe.medicine.title}</td>
                    <td><fmt:formatDate value="${recipe.expDate}" pattern="dd-MM-yyyy"/></td>
                    <td>${recipe.valid}</td>
                    <td><c:if test="${recipe.requested != true}">
                        <c:if test="${recipe.valid != true}">
                            <form method="post"
                                  action="${pageContext.request.contextPath}/controller?command=request_recipe">
                                <input type="date" name="date" value="${now}" min= <fmt:formatDate value="${now}"
                                                                                                   pattern="yyyy-MM-dd "/>>
                                <button type="submit" name="request" value="${recipe.id}"><fmt:message
                                        key="label.request"/></button>
                            </form>
                        </c:if>
                    </c:if>
                        <c:if test="${recipe.requested == true}">
                            <fmt:message key="label.requested"/>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
            <tr>
            <td class="round-bottom" colspan="4">
            <c:if test="${numberOfPages gt 1}">
                <c:if test="${currentPage != 1}">
                    <a href="controller?command=show_recipes&page=${currentPage - 1}">←</a>
                </c:if>
                <c:forEach begin="1" end="${numberOfPages}" var="i">
                    <c:choose>
                        <c:when test="${currentPage eq i}">
                            ${i}
                        </c:when>
                        <c:otherwise>
                            <a href="controller?command=show_recipes&page=${i}">${i}</a>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                <c:if test="${currentPage lt numberOfPages}">
                    <a href="controller?command=show_recipes&page=${currentPage + 1}">→</a>
                </c:if>
                </td>
                </tr>
            </c:if>
        </c:if>
        <c:if test="${fn:length(recipes) == 0}">
        <tr>
            <td class="round-bottom" colspan="4">
                    <fmt:message key="label.emptyrecipes"/>
                </c:if>
        </tr>
    </table>
</div>
<div class="category-wrap">
    <h3><fmt:message key="label.menu"/></h3>
    <ul>
        <li><a href="${pageContext.request.contextPath}/web-page/fill_balance.jsp"><fmt:message key="label.balance"/>
            (${sessionScope.user.money})</a>
        <li><a href="${pageContext.request.contextPath}/controller?command=available_medicine"><fmt:message
                key="label.home"/></a>
        <li><a href="${pageContext.request.contextPath}/controller?command=show_orders"><fmt:message
                key="label.orders"/></a>
        <li><a href="${pageContext.request.contextPath}/controller?command=show_archive_orders"><fmt:message
                key="label.archive"/></a>
        <li><a href="${pageContext.request.contextPath}/controller?command=show_recipes"><fmt:message
                key="label.recipes"/></a>
        <li><a href="${pageContext.request.contextPath}/controller?command=logout"><fmt:message key="label.logout"/></a>
    </ul>
</div>
</body>
</html>