package by.epam.pharmacy.web.command.customer;

import by.epam.pharmacy.entity.User;
import by.epam.pharmacy.service.CustomerService;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.service.validator.Validator;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;
import by.epam.pharmacy.web.command.UserIdGetter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Command for filling users balance with validation of summ
 */
public class FillBalanceCommand implements Command {
    private ServiceFactory serviceFactory;
    private Validator validator;

    public FillBalanceCommand(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
        validator = new Validator();
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String amount = request.getParameter("amount");
        if (validator.validateMoney(amount)) {
            int id = UserIdGetter.getUserId(request);
            CustomerService customerService = serviceFactory.createCustomerService();
            customerService.pay(amount, id);
            User user = customerService.getUserById(id).get();
            HttpSession session = request.getSession(false);
            session.setAttribute("user", user);
            return new CommandResult("web-page/fill_balance.jsp", false);
        } else {
            throw new IllegalArgumentException("Incorrect for money :" + amount);
        }
    }
}
