<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="/web-page/header.jsp"%>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="user"/>

<html>
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/pages.css">
   <title><fmt:message key="label.header" /></title>
</head>
<body>
<div class = "content">
 <form onSubmit='return confirm("<fmt:message key="label.confirm"/> ?");' class = "form" method = "post" action= "${pageContext.request.contextPath}/controller?command=fill_balance">
  <div class="form-inner">
  <h2><fmt:message key="label.fbalance" /></h2>
      <div class="form-content">
      <h3><fmt:message key="label.sum" /></h3>
      <p>
          <input type="number" name="amount" id="amount" placeholder=
          <fmt:message key="label.amount"/> required min="0.01" step="0.01" max="999.99"></p>
  </p>
   <p><input type="submit" value=<fmt:message key="label.pay"/>></p>

         </div>
             </div>
         </form>
 </div>

 <div class="category-wrap">
 <h3><fmt:message key="label.menu" /></h3>
 <ul>
 <li><a href="${pageContext.request.contextPath}/web-page/fill_balance.jsp"><fmt:message key="label.balance" /> (${sessionScope.user.money})</a>
 <li><a href="${pageContext.request.contextPath}/controller?command=available_medicine"><fmt:message key="label.home" /></a>
 <li><a href="${pageContext.request.contextPath}/controller?command=show_orders"><fmt:message key="label.orders" /></a>
 <li><a href="${pageContext.request.contextPath}/controller?command=show_archive_orders"><fmt:message key="label.archive" /></a>
 <li><a href="${pageContext.request.contextPath}/controller?command=show_recipes"><fmt:message key="label.recipes" /></a>
 <li><a href="${pageContext.request.contextPath}/controller?command=logout"><fmt:message key="label.logout" /></a>
 </ul>
</div>
</body>
</html>