package by.epam.pharmacy.web.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * My tag for calculating sum of order
 */
public class SumCounter extends TagSupport {
    private BigDecimal price;
    private int quantity;

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public int doStartTag() throws JspException {
        BigDecimal orderSum = price.multiply(new BigDecimal(quantity));
        try {
            pageContext.getOut().print(orderSum + "$");
        } catch (IOException ioException) {
            throw new JspException("Error: " + ioException.getMessage());
        }
        return SKIP_BODY;
    }
}
