package by.epam.pharmacy.web.command.admin;

import by.epam.pharmacy.entity.User;
import by.epam.pharmacy.service.AdminService;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;
import by.epam.pharmacy.web.command.Paginator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Command that shows all users
 */
public class ShowUsersCommand implements Command {
    private ServiceFactory serviceFactory;
    private Paginator<User> paginator;

    public ShowUsersCommand(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
        paginator = new Paginator<>();
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response)
            throws ServiceException {
        AdminService adminService = serviceFactory.createAdminService();
        List<User> users = adminService.getUsers();
        int page = 1;
        int recordsPerPage = 5;
        if (request.getParameter("page") != null)
            page = Integer.parseInt(request.getParameter("page"));
        int numberOfRecords = users.size();
        List<User> pageList = paginator.paginate(users, page, recordsPerPage);
        int numberOfPages = paginator.countPages(numberOfRecords, recordsPerPage);
        request.setAttribute("numberOfPages", numberOfPages);
        request.setAttribute("currentPage", page);
        request.setAttribute("users", pageList);
        CommandResult commandResult = new CommandResult("/web-page/admin.jsp", true);
        return commandResult;
    }
}
