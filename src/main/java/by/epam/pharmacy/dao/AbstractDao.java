package by.epam.pharmacy.dao;


import by.epam.pharmacy.dao.connection.ProxyConnection;
import by.epam.pharmacy.entity.Identifiable;
import by.epam.pharmacy.entity.builder.Builder;
import by.epam.pharmacy.entity.builder.BuilderFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * abstract DAO class
 */
public abstract class AbstractDao<T extends Identifiable> implements Dao<T> {

    private ProxyConnection proxyConnection;

    public AbstractDao(ProxyConnection proxyConnection) {
        this.proxyConnection = proxyConnection;
    }

    /**
     * @param query   sql query
     * @param builder entity builder
     * @param params  some params for query
     * @return list of entities
     * @throws DaoException when something wrong with executing query
     */
    protected List<T> executeQuery(String query, Builder<T> builder, String... params) throws DaoException {

        try {
            PreparedStatement statement = proxyConnection.prepareStatement(query);
            setPreparedStatement(statement, params);
            ResultSet resultSet = statement.executeQuery();
            List<T> entities = new ArrayList<>();
            while (resultSet.next()) {
                T entity = builder.build(resultSet);
                entities.add(entity);
            }
            return entities;
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        } finally {
            proxyConnection.close();
        }
    }

    /**
     * updates info in database
     *
     * @param query  sql query
     * @param params some params for query
     * @throws DaoException when something wrong with executing query
     */
    protected void executeUpdate(String query, String... params) throws DaoException {

        try {
            PreparedStatement statement = proxyConnection.prepareStatement(query);
            setPreparedStatement(statement, params);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        } finally {
            proxyConnection.close();
        }
    }

    @Override
    public List<T> getAll() throws DaoException {
        String table = getTableName();
        return executeQuery("SELECT * FROM " + table, BuilderFactory.getBuilder(table));
    }

    @Override
    public Optional<T> getById(int id) throws DaoException {
        String table = getTableName();
        return executeForSingleResult("SELECT * FROM " + table, BuilderFactory.getBuilder(table), Integer.toString(id));
    }

    protected Optional<T> executeForSingleResult(String query, Builder<T> builder, String... params) throws DaoException {
        List<T> items = executeQuery(query, builder, params);
        if (items.size() == 1) {
            return Optional.of(items.get(0));
        } else {
            return Optional.empty();
        }
    }

    protected abstract String getTableName();

    private void setPreparedStatement(PreparedStatement statement, String... array) throws DaoException {
        for (int i = 0; i < array.length; i++) {
            try {
                statement.setString(i + 1, array[i]);
            } catch (SQLException e) {
                throw new DaoException("Error preparing statement!!", e);
            }
        }
    }
}





