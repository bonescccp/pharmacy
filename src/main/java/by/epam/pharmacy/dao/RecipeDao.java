package by.epam.pharmacy.dao;

import by.epam.pharmacy.entity.Identifiable;
import by.epam.pharmacy.entity.Recipe;

import java.util.Date;
import java.util.List;

/**
 * DAO interface for recipes
 */
public interface RecipeDao<T extends Identifiable> extends Dao<T> {
    /**
     * returns recipes of user (by id)
     *
     * @param id id of user
     * @return list of users recipes
     * @throws DaoException when something wrong with executing query
     */
    List<Recipe> getUserRecipes(int id) throws DaoException;

    /**
     * gives recipe to user for some medicine
     *
     * @param userId     id of user
     * @param medicineId id of medicine
     * @param date       date when recipe expires
     * @throws DaoException when something wrong with executing query
     */
    void giveRecipe(String userId, String medicineId, String date) throws DaoException;

    /**
     * extends recipe to some date
     *
     * @param recipeId id of recipe request
     * @param date     date of extension
     * @throws DaoException when something wrong with executing query
     */
    void extendRecipe(int recipeId, Date date) throws DaoException;


    List<T> getAll() throws DaoException;
}


