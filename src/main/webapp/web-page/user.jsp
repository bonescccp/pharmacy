<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/web-page/header.jsp" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="user"/>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/pages.css">
    <title><fmt:message key="label.header"/></title>
</head>
<body>
<div class="content">
    <table class="table">
        <tr>
            <c:if test="${fn:length(available_medicines) gt 0}">
            <th><fmt:message key="label.title"/></th>
            <th><fmt:message key="label.price"/></th>
            <th><fmt:message key="label.form"/></th>
            <th><fmt:message key="label.order"/></th>
        </tr>

        <c:forEach var="medicine" items="${available_medicines}">
            <tr>
                <td>${medicine.title}</td>
                <td>${medicine.price}$</td>
                <td><c:choose>
                    <c:when test="${medicine.form == 'PILL'}">
                        <fmt:message key="label.pill"/>
                    </c:when>
                    <c:when test="${medicine.form == 'CAPSULE'}">
                        <fmt:message key="label.capsule"/>
                    </c:when>
                    <c:when test="${medicine.form == 'INJECTION'}">
                        <fmt:message key="label.injection"/>
                    </c:when>
                    <c:when test="${medicine.form == 'SYRUP'}">
                        <fmt:message key="label.syrup"/>
                    </c:when>
                </c:choose></td>
                <td>
                    <form method="post" action="${pageContext.request.contextPath}/controller?command=order">
                        <input type="number" name="qty" id="qty" placeholder=
                            <fmt:message key="label.qty"/> required min="1" step="1" max="99">
                        <button type="submit" name="med" value="${medicine.id}"><fmt:message
                                key="label.order"/></button>
                    </form>
                </td>
            </tr>
        </c:forEach>
        <tr>
            <td class="round-bottom" colspan="4">
                <c:if test="${numberOfPages gt 1}">
                <c:if test="${currentPage != 1}">
                    <a href="controller?command=available_medicine&page=${currentPage - 1}">←</a>
                </c:if>
                <c:forEach begin="1" end="${numberOfPages}" var="i">
                    <c:choose>
                        <c:when test="${currentPage eq i}">
                            ${i}
                        </c:when>
                        <c:otherwise>
                            <a href="controller?command=available_medicine&page=${i}">${i}</a>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                <c:if test="${currentPage lt numberOfPages}">
                    <a href="controller?command=available_medicine&page=${currentPage + 1}">→</a
                </c:if>
            </td>
        </tr>
        </c:if>
        </c:if>
        <c:if test="${fn:length(available_medicines) == 0}">
        <tr>
            <td class="round-bottom" colspan="4">
                    <fmt:message key="label.emptymedecines"/>
                </c:if>
        </tr>
    </table>
</div>
<div class="category-wrap">
    <h3><fmt:message key="label.menu"/></h3>
    <ul>
        <li><a href="${pageContext.request.contextPath}/web-page/fill_balance.jsp"><fmt:message key="label.balance"/>
            (${sessionScope.user.money})</a>
        <li><a href="${pageContext.request.contextPath}/controller?command=available_medicine"><fmt:message
                key="label.home"/></a>
        <li><a href="${pageContext.request.contextPath}/controller?command=show_orders"><fmt:message
                key="label.orders"/></a>
        <li><a href="${pageContext.request.contextPath}/controller?command=show_archive_orders"><fmt:message
                key="label.archive"/></a>
        <li><a href="${pageContext.request.contextPath}/controller?command=show_recipes"><fmt:message
                key="label.recipes"/></a>
        <li><a href="${pageContext.request.contextPath}/controller?command=logout"><fmt:message key="label.logout"/></a>
    </ul>
</div>
</body>
</html>