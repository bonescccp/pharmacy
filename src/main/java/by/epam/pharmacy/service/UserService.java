package by.epam.pharmacy.service;

import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.DaoFactory;
import by.epam.pharmacy.dao.UserDao;
import by.epam.pharmacy.entity.User;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.Optional;

/**
 * Service for unsigned users
 */
public class UserService {

    private DaoFactory daoFactory;

    public UserService(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    public Optional<User> findUserByLoginAndPassword(String login, String password) throws ServiceException {
        try {
            UserDao userDao = daoFactory.createUserDaoImpl();
            String mdPassword = hashPassword(password);
            return userDao.findUserByLoginAndPassword(login, mdPassword);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    /**
     * method for hashing password for secure storing in database
     *
     * @param password password
     * @return md5 hash
     */
    private String hashPassword(String password) {
        String mdPassword = DigestUtils.md5Hex(password);
        return mdPassword;
    }
}
