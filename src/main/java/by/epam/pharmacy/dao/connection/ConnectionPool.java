package by.epam.pharmacy.dao.connection;

import java.io.IOException;
import java.sql.Connection;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Class for managing connections
 */
public final class ConnectionPool {
    private static final String FILE_NAME = "database_config.properties";
    private static final ReentrantLock LOCK = new ReentrantLock();

    private static ConnectionPool instance;
    private static AtomicBoolean isActive = new AtomicBoolean();

    private BlockingQueue<ProxyConnection> connectionPoolQueue;
    private BlockingQueue<ProxyConnection> busyConnections;
    private ConnectionFactory connectionFactory;
    private int poolSize;

    private ConnectionPool() {
        DataBaseConfig dataBaseConfig;
        ClassLoader classLoader = getClass().getClassLoader();
        String path = Objects.requireNonNull(classLoader.getResource(FILE_NAME)).getPath();
        ConfigFactory configFactory = new ConfigFactory(path);
        try {
            dataBaseConfig = configFactory.getDatabaseConfig();
        } catch (IOException e) {
            throw new RuntimeException("Could not create pool");
        }
        connectionFactory = new ConnectionFactory(dataBaseConfig);
        poolSize = dataBaseConfig.getPoolSize();
        connectionPoolQueue = new LinkedBlockingQueue<>(poolSize);
        busyConnections = new LinkedBlockingQueue<>(poolSize);
        isActive.set(true);
        initPool();
    }

    public static ConnectionPool getInstance() {
        if (!isActive.get()) {
            try {

                LOCK.lock();
                if (instance == null) {
                    instance = new ConnectionPool();
                }
                isActive.set(true);
            } finally {
                LOCK.unlock();
            }
        }
        return instance;
    }

    /**
     * initialises pool
     * creates connections to database
     * number of connections- poolSize
     */
    private void initPool() {
        for (int i = 0; i < poolSize; i++) {
            Connection connection = null;
            connection = connectionFactory.getConnection();
            ProxyConnection proxyConnection = new ProxyConnection(connection);
            connectionPoolQueue.add(proxyConnection);
        }
    }

    public ProxyConnection getConnection() {
        try {
            ProxyConnection connection = connectionPoolQueue.take();
            busyConnections.add(connection);
            return connection;
        } catch (InterruptedException e) {
            throw new ConnectionToDataBaseException(e);
        }
    }

    public void putConnection(ProxyConnection connection) {
        try {
            if (busyConnections.remove(connection)) {
                connectionPoolQueue.put(connection);
            }
        } catch (InterruptedException e) {
            throw new ConnectionToDataBaseException("Error putting connection", e);
        }
    }

    public void closePool() {
        if (!busyConnections.isEmpty()) {
            for (ProxyConnection connection : busyConnections) {
                connection.finalClose();
            }
        }
        for (int i = 0; i < poolSize; i++) {
            try {
                connectionPoolQueue.take().finalClose();
            } catch (InterruptedException e) {
                throw new ConnectionToDataBaseException("Error closing pool!", e);
            }
        }
    }
}