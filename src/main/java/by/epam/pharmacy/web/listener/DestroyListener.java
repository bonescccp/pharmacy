package by.epam.pharmacy.web.listener;

import by.epam.pharmacy.dao.connection.ConnectionPool;
import by.epam.pharmacy.dao.connection.ConnectionToDataBaseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class DestroyListener implements ServletContextListener {
    private static Logger logger = LogManager.getLogger(DestroyListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        try {
            ConnectionPool.getInstance().closePool();
        } catch (ConnectionToDataBaseException e) {
            logger.error(e);
        }
    }
}
