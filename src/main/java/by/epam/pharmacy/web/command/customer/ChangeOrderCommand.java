package by.epam.pharmacy.web.command.customer;

import by.epam.pharmacy.service.CustomerService;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.service.validator.Validator;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command for changing quantity of ordered medicine in order with validation of quantity
 */
public class ChangeOrderCommand implements Command {
    private ServiceFactory serviceFactory;
    private Validator validator;

    public ChangeOrderCommand(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
        validator = new Validator();
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String orderId = request.getParameter("order_id");
        String stringQuantity = request.getParameter("qty");
        if (validator.validateQuantity(stringQuantity)) {
            CustomerService customerService = serviceFactory.createCustomerService();
            customerService.changeOrder(orderId, stringQuantity);
            String referer = request.getHeader("Referer");
            return new CommandResult(referer, false);
        } else {
            throw new IllegalArgumentException("incorrect for quantity :" + stringQuantity);
        }
    }
}
