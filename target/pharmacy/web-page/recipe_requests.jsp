<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="/web-page/header.jsp" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="doctor"/>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/pages.css">
    <title><fmt:message key="label.requestheader"/></title>
</head>
<body>
<div class="content">
    <table class="table">
        <c:if test="${fn:length(recipe_requests) gt 0}">
        <tr>
            <th><fmt:message key="label.login"/></th>
            <th><fmt:message key="label.name"/></th>
            <th><fmt:message key="label.lname"/></th>
            <th><fmt:message key="label.title"/></th>
            <th><fmt:message key="label.date"/></th>
            <th><fmt:message key="label.accept"/></th>
            <th><fmt:message key="label.deny"/></th>
        </tr>
        <c:forEach var="request" items="${recipe_requests}">
            <tr>
                <td>${request.recipe.user.login}</td>
                <td>${request.recipe.user.firstName}</td>
                <td>${request.recipe.user.lastName}</td>
                <td>${request.recipe.medicine.title}</td>
                <td><fmt:formatDate value="${request.date}" pattern="dd-MM-yyyy"/></td>
                <td>
                    <form method="post" action="${pageContext.request.contextPath}/controller?command=accept_request">
                        <button type="submit" name="request" value="${request.id}"><fmt:message
                                key="label.accept"/></button>
                    </form>
                </td>
                <td>
                    <form method="post" action="${pageContext.request.contextPath}/controller?command=deny_request">
                        <button type="submit" name="request" value="${request.id}"><fmt:message
                                key="label.deny"/></button>
                    </form>
                </td>
            </tr>
        </c:forEach>
        <tr>
            <td class="round-bottom" colspan="7">
                <c:if test="${numberOfPages gt 1}">
                <c:if test="${currentPage != 1}">
                    <a href="controller?command=show_requests&page=${currentPage - 1}">←</a>
                </c:if>
                <c:forEach begin="1" end="${numberOfPages}" var="i">
                    <c:choose>
                        <c:when test="${currentPage eq i}">
                            ${i}
                        </c:when>
                        <c:otherwise>
                            <a href="controller?command=show_requests&page=${i}">${i}</a>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                <c:if test="${currentPage lt numberOfPages}">
                    <a href="controller?command=show_requests&page=${currentPage + 1}">→</a>
                </c:if>
            </td>
            </c:if>
            </c:if>
            <c:if test="${fn:length(recipe_requests) == 0}">
        <tr>
            <td class="round-bottom" colspan="7">
                    <fmt:message key="label.emptyrequests"/>
                </c:if>
        </tr>
    </table>
</div>
<div class="category-wrap">
    <h3><fmt:message key="label.menu"/></h3>
    <ul>
        <li><a href="${pageContext.request.contextPath}/controller?command=show_requests"><fmt:message
                key="label.home"/></a>
        <li><a href="${pageContext.request.contextPath}/controller?command=show_all_recipes"><fmt:message
                key="label.srecipes"/></a>
        <li><a href="${pageContext.request.contextPath}/controller?command=show_customers"><fmt:message
                key="label.recipe"/></a>
        <li><a href="${pageContext.request.contextPath}/controller?command=logout"><fmt:message key="label.logout"/></a>
    </ul>
</div>
</body>
</html>