package by.epam.pharmacy.web.command.doctor;

import by.epam.pharmacy.entity.Medicine;
import by.epam.pharmacy.entity.User;
import by.epam.pharmacy.service.CustomerService;
import by.epam.pharmacy.service.MedicineService;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Command for showing users with role User and reciped medicines for giving recipe
 */
public class ShowCustomersCommand implements Command {
    private ServiceFactory serviceFactory;

    public ShowCustomersCommand(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response)
            throws ServiceException {
        CustomerService service = serviceFactory.createCustomerService();
        List<User> users = service.getCustomers();
        MedicineService medicineService = serviceFactory.createMedicineService();
        List<Medicine> medicines = medicineService.getRecipedMedicines();
        request.setAttribute("medicines", medicines);
        request.setAttribute("users", users);
        return new CommandResult("/web-page/doctor.jsp", true);
    }
}
