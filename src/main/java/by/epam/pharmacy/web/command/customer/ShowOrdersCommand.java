package by.epam.pharmacy.web.command.customer;

import by.epam.pharmacy.entity.Order;
import by.epam.pharmacy.service.CustomerService;
import by.epam.pharmacy.service.ServiceException;
import by.epam.pharmacy.service.ServiceFactory;
import by.epam.pharmacy.web.command.Command;
import by.epam.pharmacy.web.command.CommandResult;
import by.epam.pharmacy.web.command.Paginator;
import by.epam.pharmacy.web.command.UserIdGetter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Command for getting active orders
 */
public class ShowOrdersCommand implements Command {
    private ServiceFactory serviceFactory;
    private Paginator<Order> paginator;

    public ShowOrdersCommand(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
        paginator = new Paginator<>();
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        int id = UserIdGetter.getUserId(request);
        CustomerService customerService = serviceFactory.createCustomerService();
        List<Order> orders = customerService.getUsersOrders(id);
        int page = 1;
        int recordsPerPage = 5;
        if (request.getParameter("page") != null)
            page = Integer.parseInt(request.getParameter("page"));
        List<Order> pageList = paginator.paginate(orders, page, recordsPerPage);
        int numberOfRecords = orders.size();
        int numberOfPages = paginator.countPages(numberOfRecords, recordsPerPage);
        request.setAttribute("numberOfPages", numberOfPages);
        request.setAttribute("currentPage", page);
        request.setAttribute("orders", pageList);
        return new CommandResult("/web-page/orders.jsp", true);
    }
}

